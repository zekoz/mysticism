package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"mysticism"
	"mysticism/auth"
	"mysticism/conscription"
	"mysticism/datr"
	"mysticism/ent"
	"mysticism/ent/migrate"
	"mysticism/graph"
	"mysticism/meta"
	"mysticism/websocket"
	"net/http"
	"time"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/lru"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/docker/docker/client"
	"github.com/go-redis/redis/v8"
	"github.com/golang/glog"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

var (
	port = flag.Int("port", 8888, "port")
)

func init() {
	viper.SetConfigName("mysticism")
	viper.AddConfigPath("$HOME/.mysticism")
	viper.AddConfigPath(".")
	viper.SetDefault("civsever.repo", "https://github.com/longturn/freeciv21")
	viper.SetDefault("civserver.setupCommands", []string{"set scorelog enabled",
		"metaserver http://localhost:8888/meta",
		"metaconnection up",
		"set autosaves timer",
	})
}

func newServer(es graphql.ExecutableSchema) *handler.Server {

	if err := viper.ReadInConfig(); err != nil {
		glog.Fatal(err)
	}
	viper.WatchConfig()
	srv := handler.New(es)

	srv.AddTransport(transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
	})
	srv.AddTransport(transport.Options{})
	srv.AddTransport(auth.GET{})
	srv.AddTransport(auth.POST{})
	srv.AddTransport(transport.MultipartForm{})

	srv.SetQueryCache(lru.New(1000))

	srv.Use(extension.Introspection{})
	srv.Use(extension.AutomaticPersistedQuery{
		Cache: lru.New(100),
	})

	return srv
}

func main() {
	flag.Parse()
	ctx := context.Background()

	c, err := client.NewClientWithOpts(client.WithHost("unix:///run/fcweb/podman.sock"))
	if err != nil {
		log.Fatal(err)
	}
	mysticism.Docker = c

	mysticism.Redis = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	client, err := ent.Open("postgres", "user=fcweb dbname=fcweb host=/var/run/postgresql", ent.Log(glog.Info))
	if err != nil {
		log.Fatal(err)
	}

	mysticism.Client = client.Debug()
	if err := client.Schema.Create(ctx, migrate.WithGlobalUniqueID(true)); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	if err := conscription.RestoreState(ctx); err != nil {
		log.Fatal(err)
	}

	srv := &mysticism.Server{}
	g := newServer(graph.NewSchema(srv))

	m := http.NewServeMux()

	m.Handle("/", playground.Handler("GraphQL playground", "/graphql"))
	m.Handle("/graphql", g)
	m.Handle("/auth/session", http.HandlerFunc(auth.Session))
	m.Handle("/auth/quick", http.HandlerFunc(auth.Quick))
	m.Handle("/auth/login", http.HandlerFunc(auth.Login))
	m.Handle("/auth/register", http.HandlerFunc(auth.Register))
	m.Handle("/auth/logout", http.HandlerFunc(auth.Logout))
	m.Handle("/ws/", http.HandlerFunc(websocket.Handler))
	m.Handle("/meta", http.HandlerFunc(meta.Handler))

	dm := datr.NewMiddleware(m)

	go conscription.Start()
	glog.Infof("connect to http://localhost:%d/ for GraphQL playground", *port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), dm))
}
