/**
 * @generated SignedSource<<6351326b9465adc816e41793c98e6b6a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type infoJoinGameMutation$variables = {
  id: string;
};
export type infoJoinGameMutation$data = {
  readonly joinGame: {
    readonly id: string;
    readonly players: ReadonlyArray<{
      readonly user: {
        readonly id: string;
        readonly name: string;
      } | null;
    }> | null;
  };
};
export type infoJoinGameMutation = {
  variables: infoJoinGameMutation$variables;
  response: infoJoinGameMutation$data;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "id",
            "variableName": "id"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "Game",
    "kind": "LinkedField",
    "name": "joinGame",
    "plural": false,
    "selections": [
      (v1/*: any*/),
      {
        "alias": null,
        "args": null,
        "concreteType": "Player",
        "kind": "LinkedField",
        "name": "players",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              (v1/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "infoJoinGameMutation",
    "selections": (v2/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "infoJoinGameMutation",
    "selections": (v2/*: any*/)
  },
  "params": {
    "cacheID": "0dd71474fa40bae9f1ee7cb865a30ad1",
    "id": null,
    "metadata": {},
    "name": "infoJoinGameMutation",
    "operationKind": "mutation",
    "text": "mutation infoJoinGameMutation(\n  $id: ID!\n) {\n  joinGame(input: {id: $id}) {\n    id\n    players {\n      user {\n        id\n        name\n      }\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "35ae89594a169228db8fa479d8fe190f";

export default node;
