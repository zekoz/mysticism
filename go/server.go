package mysticism

import (
	"mysticism/ent"

	"github.com/docker/docker/client"
	"github.com/go-redis/redis/v8"
)

type Server struct {
}

var (
	Client *ent.Client
	Redis  *redis.Client
	Docker *client.Client
)
