package datr

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/ascii85"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/golang/glog"
	"github.com/spf13/viper"
)

const (
	cookieName  = "d"
	addedHeader = "recvdatr"
	maxage      = time.Hour * 24 * 365 * 2
)

func b85Decode(dst []byte, src []byte) (int, int, error) {
	for i := 0; i < len(src); i++ {
		switch src[i] {
		case 'v':
			src[i] = ';'
		case 'w':
			src[i] = '\\'
		case 'x':
			src[i] = '"'
		}
	}
	return ascii85.Decode(dst, src, true)
}

func b85Encode(dst []byte, src []byte) int {
	n := ascii85.Encode(dst, src)
	for i := 0; i < n; i++ {
		switch dst[i] {
		case ';':
			dst[i] = 'v'
		case '\\':
			dst[i] = 'w'
		case '"':
			dst[i] = 'x'
		}
	}
	return n
}

var (
	ErrExpired   = errors.New("expired")
	ErrNone      = errors.New("none")
	ErrFuture    = errors.New("future")
	ErrMalformed = errors.New("malformed")
)

func NewCookie(block cipher.Block, signing string) (*http.Cookie, error) {
	pt := make([]byte, 16)
	t := time.Now()
	ts := uint32(t.Unix())

	binary.BigEndian.PutUint32(pt, ts)
	n, err := rand.Read(pt[4:12])

	if err != nil {
		return nil, err
	}
	if n != 8 {
		return nil, fmt.Errorf("only read %d", n)
	}

	h := hmac.New(sha256.New, []byte(signing))
	h.Write(pt[0:12])
	sum := h.Sum(nil)
	n = copy(pt[12:16], sum)
	if n != 4 {
		return nil, fmt.Errorf("only copied %d", n)
	}

	ct := make([]byte, 16)
	block.Encrypt(ct, pt)

	a := make([]byte, 20)
	n = b85Encode(a, ct)
	if n != 20 {
		return nil, fmt.Errorf("only encoded %d", n)
	}

	return &http.Cookie{
		Name:    cookieName,
		Value:   string(a),
		Path:    "/",
		Expires: t.Add(maxage),
	}, nil
}

func Validate(block cipher.Block, signing string, value string) error {
	if value == "" {
		return ErrNone
	}
	ct := make([]byte, 16)
	n, _, err := b85Decode(ct, []byte(value))
	if err != nil || n != 16 {
		return ErrMalformed
	}
	pt := make([]byte, 16)
	block.Decrypt(pt, ct)

	h := hmac.New(sha256.New, []byte(signing))
	h.Write(pt[0:12])
	sum := h.Sum(nil)
	if !hmac.Equal(pt[12:16], sum[0:4]) {
		return ErrMalformed
	}

	ts := time.Unix(int64(binary.BigEndian.Uint32(pt)), 0)
	if time.Now().Add(60 * time.Second).Before(ts) {
		return ErrFuture
	}
	if time.Since(ts) > maxage {
		return ErrExpired
	}
	return nil
}

// Middleware implements an HTTP handler that adds and validates cookies.
type Middleware struct {
	key   string
	block cipher.Block
	next  http.Handler
}

func NewMiddleware(handler http.Handler) *Middleware {
	b := viper.GetString("datr.block")
	bs, err := base64.StdEncoding.DecodeString(b)
	if err != nil {
		panic(err)
	}

	c, err := aes.NewCipher(bs)
	if err != nil {
		panic(err)
	}

	return &Middleware{
		key:   viper.GetString("datr.key"),
		block: c,
		next:  handler,
	}
}

func AddHeader(r *http.Request, sentErr error) {
	if sentErr == nil {
		r.Header.Add(addedHeader, "ok")
		return
	}
	r.Header.Add(addedHeader, sentErr.Error())
}

func (m Middleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	sentErr := ErrNone
	c, err := r.Cookie(cookieName)
	if err == nil && c != nil {
		sentErr = Validate(m.block, m.key, c.Value)
		if sentErr == nil {
			AddHeader(r, sentErr)
			m.next.ServeHTTP(w, r)
			return
		}

	}
	new, err := NewCookie(m.block, m.key)
	if err != nil {
		glog.Error(err)
	}
	http.SetCookie(w, new)
	AddHeader(r, sentErr)
	m.next.ServeHTTP(w, r)
}
