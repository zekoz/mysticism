/**
 * @generated SignedSource<<8a3a85af2f7f821ff3cdb15317ecd45d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type playJoinGameMutation$variables = {};
export type playJoinGameMutation$data = {
  readonly joinGame: {
    readonly id: string;
  };
};
export type playJoinGameMutation = {
  variables: playJoinGameMutation$variables;
  response: playJoinGameMutation$data;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "Game",
    "kind": "LinkedField",
    "name": "joinGame",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "playJoinGameMutation",
    "selections": (v0/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "playJoinGameMutation",
    "selections": (v0/*: any*/)
  },
  "params": {
    "cacheID": "f3b741dc73af4c61a7f174c0fa9c4e1b",
    "id": null,
    "metadata": {},
    "name": "playJoinGameMutation",
    "operationKind": "mutation",
    "text": "mutation playJoinGameMutation {\n  joinGame {\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "f471c8865d6e65791ea1728ba931dbea";

export default node;
