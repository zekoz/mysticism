import type { NextPage } from 'next'
import Link from 'next/link'
import { Button } from 'antd';
import styles from '../styles/Landing.module.css'
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useContext } from 'react';
import { SessionContext } from '../lib/SessionContext';

const Home: NextPage = () => {
  const { push } = useRouter();
  const { setTryMatching } = useContext(SessionContext)!;

  return (
    <>
      <Head>
        <link rel="preload" href="/banner.jpg" as="image" />
      </Head>
      <div className="banner">
        <div className={styles.wrapper}>
          <div className="logo">
            Freeciv
          </div>
          <Button onClick={() => {
            setTryMatching(true);
            push("/games");
          }} ghost className={styles.play}>
            Play
          </Button>
        </div>
      </div>
      <div className={styles.content}>
        <div className={styles.contentTitle}><h1>Build Your Civilization</h1></div>
        <div className={styles.contentInner}>
          <ul className="left">
            <li>Play online for free against humans and AI</li>
            <li>Re-enact human history or create your own</li>
          </ul>
          <ul className="right">
            <li>Community-developed Open Source Software</li>
            <li>Over 1 million games played</li>
          </ul>
        </div>
      </div>
    </>

  );
}

export default Home
