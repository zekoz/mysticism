package herodotus

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"syscall"

	"github.com/docker/go-plugins-helpers/volume"
	"github.com/golang/glog"
)

type VolumeDriver struct {
	mountPoint string

	sync.Mutex
	volumes map[string]*fuseVolume
}

var (
	_ = (volume.Driver((*VolumeDriver)(nil)))
)

type fuseVolume struct {
	mountPoint string
	game       int
}

func NewVolumeDriver(mountPoint string) *VolumeDriver {
	return &VolumeDriver{
		mountPoint: mountPoint,
		volumes:    make(map[string]*fuseVolume),
	}
}

func (d *VolumeDriver) Create(req *volume.CreateRequest) error {
	v := &fuseVolume{}

	for key, val := range req.Options {
		switch key {
		case "game":
			v.game = val
		default:
		}
	}

	d.Lock()
	d.volumes[req.Name] = v
	d.Unlock()

	return nil

}

func (d *VolumeDriver) Remove(req *volume.RemoveRequest) error {
	d.Lock()
	defer d.Unlock()

	v, ok := d.volumes[req.Name]
	if !ok {
		return fmt.Errorf("volume %s not found", req.Name)
	}

	if err := os.RemoveAll(v.mountPoint); err != nil {
		return err
	}
	delete(d.volumes, req.Name)
	return nil
}

func (d *VolumeDriver) Path(req *volume.PathRequest) (*volume.PathResponse, error) {
	d.Lock()
	defer d.Unlock()

	v, ok := d.volumes[req.Name]
	if !ok {
		return &volume.PathResponse{}, fmt.Errorf("volume %s not found", req.Name)
	}

	return &volume.PathResponse{Mountpoint: v.mountPoint}, nil
}

func (d *VolumeDriver) Mount(req *volume.MountRequest) (*volume.MountResponse, error) {
	d.Lock()
	v, ok := d.volumes[req.Name]
	if !ok {
		return &volume.MountResponse{}, fmt.Errorf("volume %s not found", req.Name)
	}
	d.Unlock()

	v.mountPoint = filepath.Join(d.mountPoint, strconv.Itoa(v.game))
	if err := os.MkdirAll(v.mountPoint, 0755); err != nil {
		glog.Error(err)
		return &volume.MountResponse{}, err
	}

	if err := Mount(context.Background(), v.mountPoint, v.game); err != nil {
		return &volume.MountResponse{}, err
	}

	return &volume.MountResponse{Mountpoint: v.mountPoint}, nil
}

func (d *VolumeDriver) Unmount(req *volume.UnmountRequest) error {
	d.Lock()
	v, ok := d.volumes[req.Name]
	defer d.Unlock()
	if !ok {
		return fmt.Errorf("volume %s not found", req.Name)
	}

	return syscall.Unmount(v.mountPoint, 0)
}

func (d *VolumeDriver) Get(req *volume.GetRequest) (*volume.GetResponse, error) {
	d.Lock()
	defer d.Unlock()

	v, ok := d.volumes[req.Name]
	if !ok {
		return &volume.GetResponse{}, fmt.Errorf("volume %s not found", req.Name)
	}

	return &volume.GetResponse{Volume: &volume.Volume{Name: req.Name, Mountpoint: v.mountPoint}}, nil
}

func (d *VolumeDriver) List() (*volume.ListResponse, error) {
	d.Lock()
	defer d.Unlock()

	var vols []*volume.Volume
	for name, v := range d.volumes {
		vols = append(vols, &volume.Volume{Name: name, Mountpoint: v.mountPoint})
	}
	return &volume.ListResponse{Volumes: vols}, nil
}

func (d *VolumeDriver) Capabilities() *volume.CapabilitiesResponse {
	return &volume.CapabilitiesResponse{Capabilities: volume.Capability{Scope: "local"}}
}
