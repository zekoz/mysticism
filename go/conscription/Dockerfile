FROM index.docker.io/library/ubuntu:latest as build
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
	ca-certificates \
	curl \
	cmake \
	ninja-build \
	clang \
	python3 \
	gettext \
	qt5-default \
	libkf5archive-dev \
	liblua5.3-dev \
	libsqlite3-dev \
	libsdl2-mixer-dev
ARG repo=https://github.com/longturn/freeciv21
ARG freeciv_commit
ARG build_type=Release

WORKDIR build

RUN curl "${repo}/archive/${freeciv_commit}.tar.gz" -Ls | \
  tar xfz - --strip-components=1 && \
  cmake . -B build -G Ninja -DCMAKE_BUILD_TYPE=${build_type} -DFREECIV_ENABLE_CLIENT=OFF -DFREECIV_ENABLE_TOOLS=OFF -DCMAKE_INSTALL_PREFIX=/usr/local && \
  cmake --build build --target install

FROM index.docker.io/library/ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y ninja-build \
	clang \
	python3 \
	gettext \
	qt5-default \
	libkf5archive-dev \
	liblua5.3-dev \
	libsqlite3-dev \
	libsdl2-mixer-dev \
	lua5.3 \
	luarocks \
	git \
	cmake \
	libssl-dev
RUN luarocks install http && luarocks install rapidjson
RUN touch /database.conf && echo MTMsMTVjMTMKPCAJLS0gVXNlIGRlYnVnLmdldGluZm8gdG8gZ2V0IGNvcnJlY3QgZmlsZStsaW5l\
IG51bWJlcnMgZm9yIGxvYWRlZCBzbmlwcGV0CjwgCWxvY2FsIGluZm8gPSBkZWJ1Zy5nZXRpbmZv\
KDEsICJTbCIpCjwgCWxvY2FsIGhhc19iaXR3aXNlLCBiaXR3aXNlID0gcGNhbGwobG9hZCgoIlxu\
Iik6cmVwKGluZm8uY3VycmVudGxpbmUrMSkuLltbcmV0dXJuIHsKLS0tCj4gCXJldHVybiB7CjE5\
LDIyYzE3CjwgCX1dXSwgaW5mby5zb3VyY2UpKQo8IAlpZiBoYXNfYml0d2lzZSB0aGVuCjwgCQly\
ZXR1cm4gYml0d2lzZQo8IAllbmQKLS0tCj4gCX0K | base64 -d | patch /usr/local/share/lua/5.3/http/bit.lua

COPY database.lua /etc/xdg/freeciv21/database.lua
COPY --from=build /usr/local/share/freeciv21 /usr/local/share/freeciv21
COPY --from=build /usr/local/bin/freeciv21-server /usr/local/bin

RUN groupadd freeciv && \
    useradd -r -g freeciv freeciv
USER freeciv

ENTRYPOINT ["/usr/local/bin/freeciv21-server"]
