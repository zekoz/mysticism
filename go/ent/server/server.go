// Code generated by entc, DO NOT EDIT.

package server

const (
	// Label holds the string label denoting the server type in the database.
	Label = "server"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldHost holds the string denoting the host field in the database.
	FieldHost = "host"
	// FieldPort holds the string denoting the port field in the database.
	FieldPort = "port"
	// FieldContainerID holds the string denoting the container_id field in the database.
	FieldContainerID = "container_id"
	// FieldGameID holds the string denoting the game_id field in the database.
	FieldGameID = "game_id"
	// EdgeGame holds the string denoting the game edge name in mutations.
	EdgeGame = "game"
	// Table holds the table name of the server in the database.
	Table = "servers"
	// GameTable is the table that holds the game relation/edge.
	GameTable = "servers"
	// GameInverseTable is the table name for the Game entity.
	// It exists in this package in order to avoid circular dependency with the "game" package.
	GameInverseTable = "games"
	// GameColumn is the table column denoting the game relation/edge.
	GameColumn = "game_id"
)

// Columns holds all SQL columns for server fields.
var Columns = []string{
	FieldID,
	FieldHost,
	FieldPort,
	FieldContainerID,
	FieldGameID,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}
