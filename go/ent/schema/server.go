package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Server holds the schema definition for the Server entity.
type Server struct {
	ent.Schema
}

// Fields of the Game.
func (Server) Fields() []ent.Field {
	return []ent.Field{
		field.String("host").Nillable().Optional(),
		field.Int("port"),
		field.String("container_id"),
		field.Int("game_id"),
	}
}

// Edges of the Game.
func (Server) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("game", Game.Type).Ref("server").Field("game_id").Required().Unique(),
	}
}

func (Server) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("game_id").Unique(),
	}
}
