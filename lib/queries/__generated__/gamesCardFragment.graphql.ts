/**
 * @generated SignedSource<<7c26af7c379fe3c228cc90cc5803d443>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type GameState = "PLANNED" | "PREGAME" | "RUNNING" | "ENDED" | "COMPLETE" | "SUSPENDED" | "%future added value";
export type GameType = "SHORTTURN" | "LONGTURN" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type gamesCardFragment$data = {
  readonly name: string | null;
  readonly id: string;
  readonly state: GameState;
  readonly createTime: any | null;
  readonly description: string | null;
  readonly type: GameType;
  readonly ruleset: string;
  readonly scheduledStart: any | null;
  readonly turn: number | null;
  readonly players: ReadonlyArray<{
    readonly type: string;
    readonly leader: string | null;
    readonly nation: string | null;
    readonly number: number;
    readonly user: {
      readonly name: string;
    } | null;
  }> | null;
  readonly " $fragmentType": "gamesCardFragment";
};
export type gamesCardFragment$key = {
  readonly " $data"?: gamesCardFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"gamesCardFragment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "gamesCardFragment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "state",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createTime",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    (v1/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "ruleset",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "scheduledStart",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "turn",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Player",
      "kind": "LinkedField",
      "name": "players",
      "plural": true,
      "selections": [
        (v1/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "leader",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "nation",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "number",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "User",
          "kind": "LinkedField",
          "name": "user",
          "plural": false,
          "selections": [
            (v0/*: any*/)
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Game",
  "abstractKey": null
};
})();

(node as any).hash = "529e48c10b9aae49cdaef64ab690d720";

export default node;
