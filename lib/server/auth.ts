import { parse } from 'cookie';
import { IncomingHttpHeaders } from 'http';
import { NextPageContext } from 'next';

export function getPropsFromCtx({req} : NextPageContext): IncomingHttpHeaders{
    return req?.headers ?? {};
}
