package schema

import (
	"mysticism/ent/enums"

	"entgo.io/ent"
	"entgo.io/ent/dialect"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/mixin"
)

// Game holds the schema definition for the Game entity.
type Game struct {
	ent.Schema
}

// Fields of the Game.
func (Game) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").Nillable().Optional(),
		field.Other("state", enums.GameState(0)).SchemaType(map[string]string{dialect.Postgres: "int"}),
		field.Other("type", enums.GameType(0)).SchemaType(map[string]string{dialect.Postgres: "int"}),
		field.String("description").Nillable().Optional(),
		field.Int("turn").Nillable().Optional(),
		field.String("build"),
		field.String("ruleset"),
		field.String("ruleset_commit"),
		field.String("build_type").Nillable().Optional(),
		field.Int("owner_id").Nillable().Optional(),
		field.String("script").Nillable().Optional(),
		field.Time("scheduled_start").Nillable().Optional(),
	}
}

// Edges of the Game.
func (Game) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("players", Player.Type),
		edge.To("server", Server.Type),
		edge.To("owner", User.Type).Field("owner_id").Unique(),
	}
}

func (Game) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixin.Time{},
	}
}
