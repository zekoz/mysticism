package conscription

import (
	"archive/tar"
	"context"
	_ "embed"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"mysticism"
	"mysticism/ent"
	"mysticism/ent/enums"
	"mysticism/ent/game"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/volume"
	"github.com/docker/docker/pkg/jsonmessage"
	"github.com/docker/go-connections/nat"
	"github.com/golang/glog"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

var (
	maxEphemeral  = flag.Int("ephemeral_games", 100, "Limit to the number of ephemeral games")
	dropInGameAge = 30 * time.Minute
)

const (
	minPort = 5000
	maxPort = 6000
)

var (
	mu            sync.Mutex
	reservedPorts = make(map[int]struct{})
	servers       = make(map[int]string)
	ErrNoPort     = errors.New("unable to pick a port")
)

func EnsureHot(ctx context.Context) error {
	gs, err := mysticism.Client.Game.Query().Where(game.And(game.TypeEQ(enums.ShortTurn), game.StateEQ(enums.Pregame), game.HasServer())).All(ctx)
	if err != nil {
		glog.Error(err)
		return err
	}
	if len(gs) != 0 {
		return nil
	}

	r := viper.GetString("civserver.commit")
	rr := viper.GetString("civserver.rulesetCommit")
	rs := viper.GetString("civserver.defaultRuleset")
	g, err := mysticism.Client.Game.Create().SetBuild(r).SetType(enums.ShortTurn).SetState(enums.Pregame).SetRuleset(rs).SetRulesetCommit(rr).Save(ctx)
	if err != nil {
		glog.Error(err)
		return err
	}

	return Spawn(ctx, g)
}

func RestoreState(ctx context.Context) error {
	ss, err := mysticism.Client.Server.Query().WithGame().All(ctx)
	if err != nil {
		return err
	}
	mu.Lock()
	for _, s := range ss {
		servers[s.Port] = s.ContainerID
		go WatchContainer(s.Port, s.ContainerID, s)
	}
	mu.Unlock()
	return nil
}

func Start() {
	err := EnsureHot(context.Background())
	if err != nil {
		glog.Error(err)
	}
}

func WatchContainer(p int, id string, srv *ent.Server) {
	ctx := context.Background()

	resultC, errC := mysticism.Docker.ContainerWait(ctx, id, container.WaitConditionNotRunning)
	select {
	case wait := <-resultC:
		if wait.Error != nil {
			glog.Error("container exit status %d: %v", wait.StatusCode, wait.Error.Message)
		} else {
			glog.Infof("container exit status %d", wait.StatusCode)
		}
	case err := <-errC:
		glog.Error(err)
	}

	if srv.Edges.Game == nil {
		glog.Errorf("No game for server %v", srv)
	} else {
		if _, err := mysticism.Client.Game.UpdateOneID(srv.Edges.Game.ID).SetState(enums.Complete).Save(ctx); err != nil {
			glog.Error(err)
		}
	}
	if err := mysticism.Client.Server.DeleteOne(srv).Exec(ctx); err != nil {
		glog.Error(err)
	}

	mu.Lock()
	delete(reservedPorts, p)
	mu.Unlock()

	EnsureHot(ctx)
}

func PickPort() (int, error) {
	mu.Lock()
	defer mu.Unlock()
	for i := minPort; i < maxPort; i++ {
		_, ok := servers[i]
		if ok {
			continue
		}
		_, ok = reservedPorts[i]
		if ok {
			continue
		}

		addr, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("localhost:%d", i))
		if err != nil {
			glog.Error(err)
			continue
		}
		l, err := net.ListenTCP("tcp", addr)
		if err == nil {
			l.Close()
			reservedPorts[i] = struct{}{}
			return i, nil
		}
	}
	return 0, ErrNoPort
}

//go:embed Dockerfile
var dockerfile []byte

//go:embed database.lua
var databaseᚐlua []byte

func Spawn(ctx context.Context, game *ent.Game) error {
	p, err := PickPort()
	if err != nil {
		return err
	}
	defer func() {
		mu.Lock()
		delete(reservedPorts, p)
		mu.Unlock()
	}()
	im, err := findOrBuildImage(ctx, game.Build, game.BuildType)
	if err != nil {
		return fmt.Errorf("build failed: %s", err)
	}
	glog.Infof("using image %s", im)

	ps, err := nat.NewPort("tcp", strconv.Itoa(p))
	if err != nil {
		return err
	}
	id := strconv.Itoa(game.ID)
	gp := fmt.Sprintf("/home/fcweb/games/%s/%s", id[len(id)-3:], id)
	if err = os.MkdirAll(gp, 0755); err != nil {
		return err
	}
	if err = exec.Command("/usr/bin/setfacl", "-m", "u:100998:rwx", gp).Run(); err != nil {
		return err
	}

	rr := game.RulesetCommit
	if rr == "" {
		rr = viper.GetString("civserver.rulesetCommit")
	}
	v, err := mysticism.Docker.VolumeCreate(ctx, volume.VolumeCreateBody{
		Driver:     "codeoflaws",
		DriverOpts: map[string]string{"revision": rr},
	})
	if err != nil {
		return err
	}
	args := []string{
		"-D/dev/null",
		"-a",
		fmt.Sprintf("-M%s", viper.GetString("civserver.meta")),
		"-m",
		fmt.Sprintf("-p%d", p),
		"--keep",
	}
	if game.Ruleset != "" {
		args = append(args, "-r", game.Ruleset)
	}
	if game.BuildType != nil && *game.BuildType == "Debug" {
		args = append(args, "-d", "debug")
	}
	c, err := mysticism.Docker.ContainerCreate(ctx, &container.Config{

		Image:        im,
		Tty:          true,
		AttachStdin:  true,
		AttachStdout: true,
		AttachStderr: true,
		OpenStdin:    true,
		ExposedPorts: map[nat.Port]struct{}{ps: {}},
		WorkingDir:   "/game",
		Env:          []string{"FREECIV_DATA_PATH=/data"},
		Volumes:      map[string]struct{}{fmt.Sprintf("%s:/data", v.Name): {}},
		Cmd:          args,
	}, &container.HostConfig{
		NetworkMode: "host",
		Mounts: []mount.Mount{
			{
				Type:   mount.TypeBind,
				Target: "/game",
				Source: gp,
			},
		},
	}, &network.NetworkingConfig{}, id)
	if err != nil {
		return err
	}

	err = mysticism.Docker.ContainerStart(ctx, c.ID, types.ContainerStartOptions{})
	if err != nil {
		return err
	}

	hr, err := mysticism.Docker.ContainerAttach(ctx, c.ID, types.ContainerAttachOptions{
		Stream: true,
		Stdin:  true,
	})
	if err != nil {
		return err
	}
	defer hr.Close()
	cmds := viper.GetStringSlice("civserver.setupCommands")
	for _, c := range cmds {
		_, err = hr.Conn.Write([]byte(c + "\n"))
		if err != nil {
			return err
		}
	}

	mu.Lock()
	servers[p] = c.ID
	mu.Unlock()

	se, err := mysticism.Client.Server.Create().SetGame(game).SetContainerID(c.ID).SetPort(p).Save(ctx)
	if err != nil {
		glog.Error(err)
	}

	go WatchContainer(p, c.ID, se)

	return nil
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func findOrBuildImage(ctx context.Context, commit string, buildType *string) (string, error) {
	glog.Infof("finding or building image %s", commit)
	bld := "Release"
	if buildType != nil {
		bld = *buildType
	}
	f := filters.NewArgs(
		filters.Arg("label", "freeciv.commit="+commit),
		filters.Arg("label", "freeciv.build_type="+bld),
	)
	ss, err := mysticism.Docker.ImageList(ctx, types.ImageListOptions{Filters: f})
	if err != nil {
		return "", err
	}
	if len(ss) > 0 {
		return ss[0].ID, nil
	}

	r, w := io.Pipe()
	tw := tar.NewWriter(w)
	go func() {
		if err := tw.WriteHeader(&tar.Header{Name: "Dockerfile", Size: int64(len(dockerfile)), Mode: 0600}); err != nil {
			log.Print(err)
		}
		if _, err = tw.Write(dockerfile); err != nil {
			log.Print(err)
		}
		if err := tw.WriteHeader(&tar.Header{Name: "database.lua", Size: int64(len(databaseᚐlua)), Mode: 0755}); err != nil {
			log.Print(err)
		}
		if _, err = tw.Write(databaseᚐlua); err != nil {
			log.Print(err)
		}

		tw.Close()
		w.Close()
	}()

	repo := viper.GetString("civserver.repo")
	ibo := types.ImageBuildOptions{
		BuildArgs: map[string]*string{
			"freeciv_commit": &commit,
			"repo":           &repo,
			"build_type":     &bld,
		},
		SuppressOutput: false,
		Labels: map[string]string{
			"freeciv.commit":     commit,
			"freeciv.build_type": bld,
		},
	}
	ibr, err := mysticism.Docker.ImageBuild(ctx, r, ibo)
	if err != nil {
		log.Fatal(err)
		return "", err
	}
	defer ibr.Body.Close()
	dec := json.NewDecoder(ibr.Body)
	var js []jsonmessage.JSONMessage

	for {
		var j jsonmessage.JSONMessage

		err := dec.Decode(&j)
		if err == io.EOF {
			break
		}
		if err != nil {
			return "", err
		}
		if j.ErrorMessage != "" {
			return "", fmt.Errorf("failed to build: %s", j.ErrorMessage)
		}
		if glog.V(2) {
			glog.Info(j.Stream)
		}
		js = append(js, j)
	}

	return strings.TrimSpace(js[len(js)-2].Stream), nil
}

func TakeHack(ctx context.Context, game *ent.Game, user *ent.User, leader string) {
	s, err := game.Server(ctx)
	if err != nil {
		glog.Errorf("cannot attach %v: %s", game, err)
	}
	if len(s) != 1 {
		glog.Errorf("cannot attach %v", game)
	}

	glog.V(2).Infof("attaching to %s", s[0].ContainerID)
	hr, err := mysticism.Docker.ContainerAttach(ctx, s[0].ContainerID, types.ContainerAttachOptions{
		Stream: true,
		Stdin:  true,
		Stdout: true,
		Stderr: true,
	})
	if err != nil {
		glog.Errorf("cannot attach %v: %s", game, err)
	}
	defer hr.Close()

	needle := fmt.Sprintf("%s has connected", user.Name)
	for {
		l, err := hr.Reader.ReadString('\n')
		if err != nil {
			glog.Errorf("error reading from container: %s", err)
			return
		}
		glog.V(2).Infof("read: %s", l)
		if strings.Contains(l, needle) {
			glog.Infof("detected connection from %s, attaching to %s", user.Name, leader)
			break
		}
	}
	_, err = hr.Conn.Write([]byte(fmt.Sprintf("take %s \"%s\"\naitoggle \"%s\"\n", user.Name, leader, leader)))
	if err != nil {
		glog.Errorf("failed to write command: %s", err)
	}
}

func JoinGame(ctx context.Context, user *ent.User, gid int, singlePlayer bool) (*ent.Game, error) {
	if gid != 0 {
		return mysticism.Client.Game.Get(ctx, gid)
	}

	if singlePlayer {
		return mysticism.Client.Game.Query().Where(game.And(game.HasServer(), game.State(enums.Pregame))).First(ctx)
	}

	gs, err := mysticism.Client.Game.Query().Where(game.And(game.HasServer(), game.State(enums.Running))).Order(ent.Asc(game.FieldTurn)).WithPlayers().Limit(5).All(ctx)
	if err != nil {
		return nil, err
	}

	for _, g := range gs {
		// First try to resume.
		for _, p := range g.Edges.Players {
			if p.ID == user.ID {
				return g, nil
			}
		}
		// Then find an AI to take.
		for _, p := range g.Edges.Players {
			if p.Type == "A.I." {
				tctx, cancel := context.WithTimeout(context.Background(), 2*time.Minute)
				go func() {
					TakeHack(tctx, g, user, p.Leader)
					cancel()
				}()
				return g, nil
			}
		}
	}

	return mysticism.Client.Game.Query().Where(game.State(enums.Pregame)).First(ctx)
}
