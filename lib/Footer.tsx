import React from 'react';
import styles from '../styles/Footer.module.css';


function Footer(props: {}): React.ReactElement {
  return (
    <footer className="footer">
      <div className={styles.tos}></div>
      <div className={styles.copy}>&copy;2022 Freeciv Web All Rights Reserved</div>
      <a href="https://github.com/longturn/freeciv21" className={styles.source}>Credits &amp; Source</a>
    </footer>
  );
}

export default Footer;
