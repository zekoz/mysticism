import Cookies from 'js-cookie';
import type { NextPage } from 'next'
import { useRouter } from 'next/router';
import { useContext, useEffect, useRef, useState } from 'react';
import { SessionContext } from '../../lib/SessionContext';

function qtSetup(canvas : HTMLCanvasElement) {
}

const Game: NextPage = () => {
    const { user, initialSessionFetched, openLoginModal } = useContext(SessionContext)!;
    const [launched, setLaunched] = useState(false);

    const qtCanvas = useRef<HTMLCanvasElement>(null);
    useEffect(() => {
        if (typeof window === 'undefined') {
            return;
        }
        if (!initialSessionFetched) {
            return;
        }
        if (!user.name && initialSessionFetched) {
            openLoginModal();
            return;
        }
        (async() => {
            const Module = (window as any).Module;
            await Module.initPromise;
            const canvas = qtCanvas.current;
            if (!canvas) {
                return;
            }
            Module.qtCanvasElements = [canvas];
            canvas.height = 2 * window.innerHeight;
            canvas.width = 2 * window.innerWidth;
            canvas.style.height = "100%";
            canvas.style.width = "100%";
            canvas.contentEditable = "true";
            canvas.oncontextmenu = (event) => event.preventDefault();
            canvas.style.outline = "0px solid transparent";
            canvas.style.caretColor = "transparent";
            canvas.style.cursor = "default";
            const pathFrags = document.location.pathname.split("/");
            const gid = pathFrags[pathFrags.length - 1];
            Module.websocket.url = `wss://ws.freecivweb.com/ws/${gid}`
            const sess = Cookies.get("sess") ?? "";
            Module.callMain(["-a", `fc21://${user.name}:${sess}@ws.freecivweb.com/ws/${gid}`]);
        })();
    }, [qtCanvas, initialSessionFetched, user]);

    return <canvas id="QtCanvas" ref={qtCanvas}/>;
}

export default Game
