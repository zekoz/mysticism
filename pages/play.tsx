import Modal from 'antd/lib/modal/Modal';
import { useRouter } from 'next/router';
import { useContext, useEffect, useState } from 'react';
import { useMutation, graphql} from 'react-relay';
import { playJoinGameMutation } from '../lib/queries/__generated__/playJoinGameMutation.graphql';
import { SessionContext } from '../lib/SessionContext';


const JOIN_GAME = graphql`
  mutation playJoinGameMutation {
    joinGame {
      id
    }
  }
`;

export default function Play()  {
    const [visible, setVisible] = useState(false)
    const [joinGame, isJoinInFlight] = useMutation<playJoinGameMutation>(JOIN_GAME);
    const { user, initialSessionFetched, openLoginModal } = useContext(SessionContext)!;
    const { push } = useRouter();

    useEffect(() => {
        if (!initialSessionFetched) {
            return;
        }
        if (user && user.name) {
            setVisible(true);
            joinGame({
                variables: {},
                onCompleted: (({joinGame}, err) => {
                    if (joinGame.id) {
                        setVisible(false);
                        push(`/game/${joinGame.id}`);
                    }
                }),
            });
            return;
        }

        openLoginModal();
    },
        [initialSessionFetched, user],
    )

    return (
        <Modal visible={visible}>
            Finding game to join...
        </Modal>
    );
};
