// src/pages/_document.tsx
import { createRelayDocument, RelayDocument } from 'relay-nextjs/document';
import NextDocument, {
  Html,
  Head,
  DocumentContext,
  Main,
  NextScript,
} from 'next/document';

interface DocumentProps {
  relayDocument: RelayDocument;
}

const style = `
@font-face {
  font-family: 'FCDisplay';
  src: url(data:;base64,d09GMgABAAAAAAucABEAAAAAF+wAAAs/AAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGi4bgTgcNgZgAFQIgSgJnAwRCAqFLIRpATYCJAMcCxAABCAFg3IHIAyBVhtyFqOiTo9W5ihK4mIB8FcHtjHp8J91Mm4rNY2oNI28MbKUUlF29/JR/umfvKnbwQPRaf9mdnbD0ulBzLVbQOeckPYTLs7gz9JalmRnFumufIBcYHCi78wLWegfdb0fLCogBY5yAySHyCEtBUH6KGWdLepX1O6qkcOmytL+EkxEliznwbdsVyBIHrS3zdPj80Nja7fcgFbM3tX//7nSvvt/MqgKdDyg8IB2ha4x0z+TnvlJBpeSJWRVBIdqz8oSkCrretC1rla3EhjFxsiFr09tEjzdzYYIJP4IOx4ZDOHKFQZCTwcnzRBwdTtdGIQyU7OLiR71hHcSzx3r9UycO2lE3ZQmKID3lQ4TPd4pWMEyD4WUwqVL3OjSTKoxtP7E4b6zvtO2ObtBtBMiRByaoaeFFEMKJqG1PWE3evAWvIWzRQ9xc6U+Zw066KGqHvg0Phm5gG6yENpQ8MEyzKwklpBMqeXa/wPYHpeZLsR3ANpB/UPAhR+MMDQjtJ1im6ZwG85RupAimVQyKKS0bT6LI+XP4lvkW+ib72vy1SMQcmigctGms2rH4P+n9L98F7VrN5/WK71In6Md5K3pW3nOfO0Yn29PpExSXhf5/P9n6P3/99n+U/oN+WQo6R5ugJKA67TI0vPfYN4/R3z8K9HaJfdeNhqv36Cof2SR5XNtX5z3wD2wafMJUDZLDCCI1U2AYN58QICGb8Lavm1xhkeVEhutzWJLT21C/LAldIc4qwPsbmuiToclEp8c8SkvOJnr2xdHBNHi5Kk3K9GAs/Q1T6FSpcTqCEnA5AsJOQM9q9Mg/u5g5a8ppwYX6wXicfIIkd/9nTLUduOdcXZnlrNLnJ5W0XvPapRbB3KbZvFhWE2voemvhjYaf5tKqixWPZauqxrrUMcqOcOvAwU618BFAg5ZelvQbWy97vpl/W3LGnWL31w+ch5qBjPnXlD+gIS0mAMcWggWdRAS3yQkj8genfcZrAFxyH2esqxJwyiA6LLpuiBiFryecuEDD7CoUT73f81Wm0nDyttU3rj6LifLPovZYwLTjY3WnIsFJKDwwgQ01SQ/l+LRBCRjhYYbcARWpEFAgVp8DpPr30w+BHDarGZYYpm7K6mYlFXZXMTzfvGQ1nsxmXpJt8Y0YBMWRYsx2TQMeBxWJqWa2C6zQbMQAmXOq5mIUUSMVE5Av55M0alP8s1YFuH4TfLOfD7vAxYadY75avOADbBKWHwGnCVheczYFlzAzg040PwD89H1m8nnmGw4LBrfDMejBVw8/5AaG8/zSYyJbQU8PH+bnr9PiDl/4t6iPhq4s49BN3Z9sEWczMAmFLFZHvbVwQ0TQrxJDWWzwFza+GtVN7UH3sCmmw5DtOnZf62ZsbjCpj0UaMIGMo03bACezUECkwlVL+8twLYGdPwh4XrUAtMjhsNrVPkV3doOzrY922KMo0D+XYOt+fNF/uXuiRwI0SC4EIjEGVKUES0xgZYZoxXGaZWJtMYkWmcybTAlqwibg/Mi1ucfkqkDv18xuwZKAGeo/K1YdmaU/ymLYRtW9rUAbD9Ognr1MYqLSTBJJsWkmQyTZXJMnimwFahftF6pWrsQrZqZyZhNax5WKus2WgMimKeTMwjUIa00o3lWb9ub+8BRuhsBagSOec2cPV51F6I+aFzXF+ly7qnZHKKB4/Jb0I3WZ7axjcY5rnfvYLZHpF0Xwu8AALD1wG63lgb9nZIO1GvVBwvY5F8o0EV3oA/04Lkg11k+ax+NJDylTisgGaF4Ov6e+sqq0hfnM+hx6imu7z0fGAAIiXiEBSygV2+6LNB44etYcakfe4dVDBPVcZUQPeZ4YfF2l8TsPn1jtjvmvrGyeznHaEKlxmQuy5aF90SoB+IdL/DJdA7GMhicdTi3YawOznLsM9+DVpNgxZb1tjNULqFAZU1bLfK6kbRUWeKThkeFl2zDSkM8LKQQ095Ne9RXlAOj8VhOErxY1t9Odo4TUTHBJYmKZb04Jv4OBUb7isBV6tSbd1BCtuDTYSOGZdTT7y+Biex3xWszpvuyHzltTZhTdsxCrG66vXmay2YwdSP5l5dEmN64dzmpz12rqX8hMEckfOfejL/IP+BcJ7wJBllpzS0wzDF2EetsG8cevBYiO2q5aDsRwjVqltGvqOgeuy8WaWNTr85u1/Eb30FTpyMC4bL+J1YrWfRLvJcd2dtt59dMss9tpd0sGFwYGbln8V2SvYUt2O1hwL4ZvPTjE7zKg3vSXJ3BmhyUHKIBGmJ0fe4R6Efp+HIQLkydlmOyjVj8BIs6xrFa6xQTwM6OM4Z07kHBBVP9EodddiWIrj0ouGFa+xbfAGAz4I4j3XtR8MCVHqcDIJ440rMXBS9c9dd6sc3ecMzesc0+cKx/bthjX2Kjbw8KfhgUhmwbTK5gcg2TG5jcwuQOJvcweYBZHdGISTSSJQY7KhlcLwPvdxCFFRpwVKvgjQpqU8mS0SmiVzQHFYJGAAyTBqJZBS2pVJlYlbApYVfCoRGdKuhSIW9yjkcZrzI+Zfwa0Vy9oQp6UxX5b3VodHy7rVFvxDQqqd2+vHliTGJFRVL7equnXmuR//gHttURIRfz+bBgmgVC/k7W7lgEoHA/JNzTS0S8067CHdFPvHlsn3hh/3HHWztLinftsr95ZA/P7z1quuDuspIdOxxPBeGvAm497j4t6njCBf1yRXkeVrO4ZOvOnUPs2VuUVjrVU07l5y3xwi6hB1jvYde+UsfOvSU1jmG795c5duwpLs/0+XsEhhxOWvZ39pa9jRm95615/1puamuHpzbpQL/x/9i/+SxhecNbzUL+Wj9hU4/KwU5PUZ/D3TtvRf+wpOpaZ1XFqpyrj/dzJFUd7ttU0fRL22279Nz0qJQfY8n5I/+tmJA9c84j39vWnHnumQ8ef+vW0/k1n/yTXnzKL2Jq2Ct9y2sDj275euYE85FJPZ5x9BvWekLNtYznr8Xtp0+K7vL8lIE/vjX9Oef8ZP9zp5/t51c9g36f2Pm5CZHta/9Y1spTcanNpEQAd4dop9SkyEZKMQIhBuWgaYxQAgaTK4gMD/S3WYgTcbotvL27AxJNSW0sQjAM6JODUvowQ+h6Xz0XcOAIEpbFFpNlNa2iugjxvgo93YbNMmzR3ghPX0IKIfqKV9PQgjRbRHt3b4ShacKLgSYNbaxV6Hppz2NRUsq+MjciHKIjw10RLsIICw0LS7QVu2i8U0hEtsAnasLnAFHyi0dFKFtkewQgYPzLW/4bFjDgptWybAHYF/3GRYD9z37+pM97uE6tsN4EDCQAAv7Sido3IFz3C/eAQHxei7rKML0Kr/qOrepxtkob2/QWbNWTGYAALIzGggcdSTvKeAd4w9+JhiRZqXUWygb8DDxf0Imf50v82TZfw+TISIWbr5MqWrwbg05iAslMZRpP4GU8YxlHHS660YWu9MVFsRUecZOT5bTx1MrxFjklTXrZzsfrMMEYld8ewiNxOc5+IS+1uGjDWMbv9R9hJJ0YVZrdmYmbrHWLjsBLHbUDq2eibVtQmsLj1GnZI8jug12Ih9IfYVL55V3pRBfZsj8ZmGSR5AZW6uo7ojTixY523WwXdTJ9DCP6LqeyHI8z6sh4FC37PB/I7rStN1FDbNT3Nd24eNYc1hSWNwjxbkW9+E55nWkNWJJmwZCuiTrtDLdwO3NVquplbWWNtOh28wUKZaYcZHQyEvTDtZ1Ji0KWBC3xW2JbYrjRcQQKSsUtBV13LhqYDS3E6wXl9e7Xy200mrXJmGMl1MGsiGlozZeZZ30eodyvjypOFOKu3c58mS77GG2MaF33bzdH+F6pV+80SEjdJn20gWkCAAA=) format('woff2');
  font-style: normal;
  font-weight: 200;
  unicode-range: U+0043,U+0045,U+0046,U+0049,U+0052,U+0056;
}

@font-face {
  font-family: 'Commissioner';
  font-style: normal;
  font-weight: 200;
  font-display: block;
  src: url(/vCommissioner.woff2) format('woff2');
  unicode-range: U+0000-024F, U+0131, U+0152-0153, U+0259, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+1C80-1C88, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+20B4, U+2DE0-2DFF, U+2113, U+2C60-2C7F, U+A720-A7FF, U+2000-206F, U+2074, U+20A0-20AC, U+2116, U+2122, U+2191, U+2193, U+2212, U+2215, U+0370-03FF, U+0400-052F, U+0490-0491, U+04B0-04B1, U+A640-A69F, U+FE2E-FE2F, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Commissioner';
  font-style: normal;
  font-weight: 400;
  font-display: block;
  src: url(/vCommissioner.woff2) format('woff2');
  unicode-range: U+0000-024F, U+0131, U+0152-0153, U+0259, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+1C80-1C88, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+20B4, U+2DE0-2DFF, U+2113, U+2C60-2C7F, U+A720-A7FF, U+2000-206F, U+2074, U+20A0-20AC, U+2116, U+2122, U+2191, U+2193, U+2212, U+2215, U+0370-03FF, U+0400-052F, U+0490-0491, U+04B0-04B1, U+A640-A69F, U+FE2E-FE2F, U+FEFF, U+FFFD;
}

html,
body {
    padding: 0;
    margin: 0;
    font-family: Commissioner;
    font-weight: 400;
    background-color: #F2EBDC;
    width: 100%;
    color: #260E0B;
}

input, textarea {
  font-family: Commissioner;
}

.logo {
  font-weight: 200;
  text-transform: uppercase;
  font-size: calc(4vw + 2vh);
  color: #F2A71B;
  position: relative;
  padding-left: 0.5em;
  letter-spacing: 0.5em;
  font-family: FCDisplay;
  text-shadow: 0 0 4px #260E0B;
}

.flex {
  flex-direction: column;
  display: flex;
  min-height: 100vh;
}

.footer {
  margin-top: auto;
  width: 100%;
  background-color: #260E0B;
  display: flex;
  color: #F2A71B;
  justify-content: space-between;
  align-items: center;
  font-size: 0.8rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
}

.header {
  background-color: #260E0B;
  display: flex;
  color: #F2A71B;
  justify-content: space-between;
  align-items: center;
}

.headerLogo {
  font-weight: 200;
  text-transform: uppercase;
  font-size: 3vh;
  padding-left: 3vh;
  padding-top: 1vh;
  padding-bottom: 0.5vh;
  letter-spacing: 0.5em;
}

.rightContent {
  padding-right: 3vh;
}

.banner {
  background: radial-gradient( rgba(0, 0, 0, 0.7) 50%, rgba(0, 0, 0, 0.1)), url(/banner.jpg);
  background-repeat: no-repeat;
  background-size: 100%;
  background-clip: padding-box, padding-box;
  background-attachment: scroll;
  height: min(70vw, 100vh);
  text-align: center;
}

.fullSize {
  height: 100%;
  width: 100%;
}
`;

const pixel = `!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '684097825931815');
fbq('track', 'PageView');`;

const scriptInit = `
Module = {};
Module.locateFile = function(path, prefx) {
  return "/" + path;
}
Module.noInitialRun = true;
Module.initPromise = new Promise((resolve) => {
  Module.onRuntimeInitialized = resolve;
});
`;

class MysticismDocument extends NextDocument<DocumentProps> {
  static async getInitialProps(ctx: DocumentContext) {
    const relayDocument = createRelayDocument();

    const renderPage = ctx.renderPage;
    ctx.renderPage = () =>
      renderPage({
        enhanceApp: (App) => relayDocument.enhance(App),
      });

    const initialProps = await NextDocument.getInitialProps(ctx);

    return {
      ...initialProps,
      relayDocument,
    };
  }

  render() {
    const { relayDocument } = this.props;

    return (
      <Html>
        <Head>
          <style
            dangerouslySetInnerHTML={{
              __html: style,
            }} />
          <script
            dangerouslySetInnerHTML={{
              __html: scriptInit,
            }}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: pixel,
            }}
          />
          <script defer
            src="https://static.cloudflareinsights.com/beacon.min.js"
            data-cf-beacon='{"token": "f6e8e9ce1a89453c94db0b7f2f869bc1"}'>
          </script>
          <relayDocument.Script />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MysticismDocument;
