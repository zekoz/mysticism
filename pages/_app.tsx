import { getInitialPreloadedQuery, getRelayProps } from 'relay-nextjs/app';
import { getClientEnvironment } from '../lib/client_environment';
import { RelayEnvironmentProvider } from 'react-relay/hooks';
import type { AppProps } from 'next/app'
import Script from 'next/script'
import '../styles/globals.css'
import '../styles/antdatepicker.css'
import '../styles/antinput.css'
import '../styles/antmenu.css'
import '../styles/antmodal.css'
import '../styles/antform.css'
import '../styles/antbutton.css'
import '../styles/anttable.css'
import '../styles/anttypography.css'
import '../styles/antspin.css'
import '../styles/antmotion.css'
import Header from '../lib/Header';
import Footer from '../lib/Footer';
import type { CurrentUser, Session } from '../lib/SessionContext';
import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { SessionContext } from '../lib/SessionContext';
import { Button, Form, Input, Modal, Typography } from 'antd';
import { useContext } from 'react';
import HCaptcha from '@hcaptcha/react-hcaptcha';
import { useRouter } from 'next/router';
const clientEnv = getClientEnvironment();
const initialPreloadedQuery = getInitialPreloadedQuery({
  createClientEnvironment: () => getClientEnvironment()!,
});

type ModalProps = {
  visible: boolean,
  hide: () => void,
  register: boolean,
  setRegister: Dispatch<SetStateAction<boolean>>,
  loading: boolean,
  setLoading: Dispatch<SetStateAction<boolean>>,
};

function LoginModal({ visible, hide, register, setRegister, loading, setLoading }: ModalProps) {
  const { setUser } = useContext(SessionContext)!;
  const [verification, setVerification] = useState<string | undefined>(undefined);
  const [reason, setReason] = useState<string | null>(null);
  const [form] = Form.useForm();

  const handleOk = () => {
    (async () => {
      setLoading(true);
      const entry = await form.validateFields();
      const resp = await fetch(`/auth/${register ? "register" : "login"}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        mode: "same-origin",
        credentials: "include",
        body: JSON.stringify({
          ...entry,
          verification,
        }),
      });
      if (!resp.ok) {
        const reason = await resp.text();
        setReason(reason);
        setLoading(false);
        return;
      }

      const js = (await resp.json()) as CurrentUser;
      setUser(js);
      hide();
      setLoading(false);
      form.resetFields();
      if (register) { fbq("track", "CompleteRegistration"); }
    })();
  };

  return (
    <Modal
      title={register ? "Get started playing" : "Log in to Freeciv"}
      visible={visible}
      okText={register ? "Register" : "Log in"}
      cancelText="Cancel"
      footer={<>
        {reason && <div className="errorMessage"><Typography.Text type="danger">{reason}</Typography.Text></div>}
        <Button key="back" onClick={hide}>
          Return
        </Button>
        <Button key="submit" type="primary" loading={loading} onClick={handleOk}>
          Submit
        </Button>
      </>}
      onCancel={hide}
      onOk={handleOk}
    >
      <Form
        form={form}
        layout="vertical"
        name="login"
        initialValues={{}}
        preserve={false}
      >
        <Form.Item
          label="Username"
          name="username"
          preserve={false}
          rules={[{ required: true, message: 'Please input your username' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          preserve={false}
          rules={[{ required: true, message: 'Please input your password' }]}
        >
          <Input.Password />
        </Form.Item>
        {register && <>
          <Form.Item
            label="Email address"
            name="email"
            preserve={false}
            rules={[{ required: true, message: 'Please input your email address' }]}
          >
            <Input />
          </Form.Item>
          <HCaptcha onVerify={token => setVerification(token)} sitekey="299eea6b-a387-4eeb-9020-f74e1614f9f8" />
        </>
        }
        <Typography.Link onClick={() => setRegister(!register)}>
          {register ? "Already have an account? Log in" : "Sign up"}
        </Typography.Link>
      </Form>
    </Modal >
  );
}

function MysticismApp({ Component, pageProps }: AppProps) {
  const [user, setUser] = useState<CurrentUser>({});
  const [initialSessionFetched, setInitialSessionFetched] = useState(false);

  useEffect(() => {
    (async () => {
      if (typeof window === undefined) {
        return;
      }
      const resp = await fetch('/auth/session');
      const sess = (await resp.json()) as CurrentUser;
      setUser(sess);
      setInitialSessionFetched(true);
    })();
  }, []);

  const logout = () => {
    (async () => {
      const resp = await fetch('/auth/logout', {
        method: "POST",
        credentials: "include",
      });
      setUser({});
    })();
  }

  const [visible, setVisible] = useState(false);
  const [register, setRegister] = useState(true);
  const [loading, setLoading] = useState(false);

  const {pathname} = useRouter();
  const hideHeader = pathname === "/game/[gid]";

  const [tryMatching, setTryMatching] = useState(false);

  const ctx = {
    user,
    setUser,
    openLoginModal: () => setVisible(true),
    logout,
    loading,
    setLoading,
    initialSessionFetched,
    tryMatching,
    setTryMatching,
  };

  const relayProps = getRelayProps(pageProps, initialPreloadedQuery);
  const env = relayProps.preloadedQuery?.environment ?? clientEnv!;
  return (
    <>
      <Script src="/qtloader.js" strategy="afterInteractive" />
      <Script src="https://js.hcaptcha.com/1/api.js" strategy="afterInteractive" />
      <SessionContext.Provider value={ctx}>
        <RelayEnvironmentProvider environment={env}>
          <LoginModal
            visible={visible}
            register={register}
            setRegister={setRegister}
            hide={() => setVisible(false)}
            loading={loading}
            setLoading={setLoading}
          />
          <div className="flex">
            {!hideHeader && <Header />}
            <Component {...pageProps} {...relayProps} />
            {!hideHeader && <Footer />}
          </div>
        </RelayEnvironmentProvider>
      </SessionContext.Provider>

    </>
  );
}

export default MysticismApp
