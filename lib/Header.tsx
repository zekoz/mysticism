import React, { useContext } from 'react';
import Link from 'next/link'
import { Menu } from 'antd';
import styles from '../styles/Header.module.css';
import { SessionContext } from './SessionContext';


const { Item, SubMenu } = Menu;

const AccountMenu : React.ComponentType = () => {
  const {user, openLoginModal, logout} = useContext(SessionContext)!;

  const loggedIn = user.name ? user.name.match(/~/) == null : false;

  return loggedIn ? <a onClick={logout}>Log Out</a> : <a onClick={openLoginModal}>Sign Up</a>
}

function Header(props: {}): React.ReactElement {
  return (
    <header className={styles.header}>
      <div className={styles.headerLogo}><Link href="/">Freeciv</Link></div>
      <Menu className={styles.rightContent}
        mode="horizontal">
        <Menu.Item key="games">
          <Link href="/games">Games</Link>
        </Menu.Item>
        <Menu.Item key="account">
          <AccountMenu/>
        </Menu.Item>
      </Menu>
    </header>
  );
}

export default Header;
