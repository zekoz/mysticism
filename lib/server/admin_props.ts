import { GetServerSideProps, NextPageContext } from "next";
import { ComponentType } from "react";
import { fetchQuery, graphql } from "react-relay";
import { WiredOptions, WiredProps } from "relay-nextjs/wired/component";
import { Environment, GraphQLTaggedNode, RecordSource, Store } from "relay-runtime";
import { adminPropsIdentityQuery } from "../queries/__generated__/adminPropsIdentityQuery.graphql";
import { createServerNetwork } from "./server_environment";

export async function adminProps(ctx: NextPageContext) {
    const resp = await fetchQuery<adminPropsIdentityQuery>(
        new Environment({
            network: createServerNetwork(ctx?.req?.headers ?? {}),
            store: new Store(new RecordSource()),
            isServer: true,
        }),
        graphql`query adminPropsIdentityQuery{
            identity {
                memberOf {
                    edges {
                        node {
                            name
                        }
                    }
                }
            }
        }`,
        {},
    ).toPromise();

    if (!(resp?.identity?.memberOf?.edges ?? []).length) {
        return {
            props: {},
            redirect: {
                destination: "/",
            }
        }
    }

    return {
        props: {},
    }
}
