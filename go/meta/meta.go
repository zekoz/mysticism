package meta

import (
	"context"
	"mysticism"
	"mysticism/conscription"
	"mysticism/ent"
	"mysticism/ent/enums"
	"mysticism/ent/game"
	"mysticism/ent/server"
	"mysticism/ent/user"
	"net/http"
	"net/url"
	"strconv"

	"github.com/golang/glog"
)

type Player struct {
	User   string
	Leader string
	Nation string
	Type   string
}

type MetaMessage struct {
	Capability string
	Available  int
	Host       string
	Humans     int
	Message    string
	Patches    string
	Players    []Player
	Port       int
	State      string
	Ruleset    string
	Variables  map[string]string
}

func decode(vs url.Values) (*MetaMessage, error) {
	m := &MetaMessage{
		Capability: vs["capability"][0],
		Host:       vs["host"][0],
		Message:    vs["message"][0],
		Patches:    vs["patches"][0],
		State:      vs["state"][0],
		Ruleset:    vs["ruleset"][0],
		Variables:  make(map[string]string),
	}
	p, err := strconv.Atoi(vs["port"][0])
	if err != nil {
		return m, err
	}
	m.Port = p

	if len(vs["available"]) == 1 {
		a, err := strconv.Atoi(vs["available"][0])
		if err != nil {
			return m, err
		}
		m.Available = a
	}

	for i := 0; i < len(vs["vv[]"]); i++ {
		m.Variables[vs["vn[]"][i]] = vs["vv[]"][i]
	}

	m.Players = make([]Player, len(vs["plu[]"]))
	for i := range m.Players {
		m.Players[i].User = vs["plu[]"][i]
		m.Players[i].Leader = vs["pll[]"][i]
		m.Players[i].Type = vs["plt[]"][i]
		m.Players[i].Nation = vs["pln[]"][i]
	}
	return m, nil
}

var stateMap = map[string]enums.GameState{
	"Pregame":    enums.Pregame,
	"Game Ended": enums.Ended,
	"Running":    enums.Running,
}

func Handler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if glog.V(2) {
		glog.Infof("received raw: %v", r.PostForm)
	}
	m, err := decode(r.PostForm)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if glog.V(2) {
		glog.Infof("received meta: %v", m)
	}

	players := map[int]Player{}
	for i, p := range m.Players {
		players[i+1] = p
	}

	needsWrite := false
	tx, err := mysticism.Client.Tx(ctx)
	if err != nil {
		glog.Errorf("failed to make transaction: %s", err)
	}

	g, err := mysticism.Client.Game.Query().Where(game.HasServerWith(server.Port(m.Port))).WithServer().WithPlayers(func(q *ent.PlayerQuery) { q.WithUser() }).Only(ctx)
	if err != nil {
		glog.Errorf("cannot find server/game for port %d", m.Port)
	}

	gs, ok := stateMap[m.State]
	if !ok {
		glog.Errorf("invalid state: %v", m)
		return
	}

	t, err := strconv.Atoi(m.Variables["turn"])
	if err != nil {
		glog.Warningf("cannot parse turn from %v", m.Variables)
	}
	var gu *ent.GameUpdateOne
	if t != 0 && (g.Turn == nil || *g.Turn != t) {
		if gu == nil {
			gu = tx.Game.UpdateOne(g)
		}
		gu.SetTurn(t)
	}

	if g.State != gs || g.Ruleset != m.Ruleset {
		if gu == nil {
			gu = tx.Game.UpdateOne(g)
		}
		gu.SetState(gs).SetRuleset(m.Ruleset)
	}

	if gu != nil {
		if err := gu.Exec(ctx); err != nil {
			tx.Rollback()
			glog.Error(err)
			return
		}
		needsWrite = true
	}

	maxFound := -1

	for _, p := range g.Edges.Players {
		if p.Number > maxFound {
			maxFound = p.Number
		}
		otherp, ok := players[p.Number]
		if !ok {
			needsWrite = true
			if err := tx.Player.DeleteOne(p).Exec(ctx); err != nil {
				tx.Rollback()
				glog.Error(err)
				return
			}
		}

		var mut *ent.PlayerUpdateOne
		if otherp.Leader != p.Leader || otherp.Nation != p.Nation || otherp.Type != p.Type {
			needsWrite = true
			mut = tx.Player.UpdateOne(p).SetLeader(otherp.Leader).SetNation(otherp.Nation).SetType(otherp.Type)
		}
		if otherp.Type == "Human" && otherp.User != "Unassigned" && (p.Edges.User == nil || p.Edges.User.Name != otherp.User) {
			u, err := tx.User.Query().Where(user.Name(otherp.User)).Only(ctx)
			if err != nil {
				tx.Rollback()
				glog.Errorf("%s: %s", err, otherp.User)
				return
			}

			if mut == nil {
				mut = tx.Player.UpdateOne(p)
			}
			mut.SetUser(u)
		} else if otherp.Type != "Human" && p.Edges.User != nil {
			if mut == nil {
				mut = tx.Player.UpdateOne(p)
			}
			mut = mut.ClearUser()
		}
		if mut != nil {
			needsWrite = true
			mut.Exec(ctx)
		}
	}

	var bulk []*ent.PlayerCreate
	for i, p := range players {
		if i > maxFound {
			create := tx.Player.Create().SetGame(g).SetLeader(p.Leader).SetNation(p.Nation).SetType(p.Type).SetNumber(i)
			if p.Type == "Human" {
				u, err := tx.User.Query().Where(user.Name(p.User)).Only(ctx)
				if err != nil {
					tx.Rollback()
					glog.Error(err)
					return
				}
				create = create.SetUser(u)
			}
			bulk = append(bulk, create)
		}
	}
	if len(bulk) > 0 {
		needsWrite = true
		err = tx.Player.CreateBulk(bulk...).Exec(ctx)
		if err != nil {
			glog.Error(err)
			tx.Rollback()
		}
	}
	if needsWrite {
		if err := tx.Commit(); err != nil {
			glog.Error(err)
		}
	}
	if g.State == enums.Pregame && m.State == "Running" {
		conscription.EnsureHot(context.Background())
	}
}
