package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"mysticism"
	"mysticism/auth"
	"mysticism/conscription"
	"mysticism/ent"
	"mysticism/ent/enums"
	"mysticism/ent/game"
	"mysticism/ent/group"
	"mysticism/ent/user"

	"github.com/spf13/viper"
)

func (r *groupResolver) Members(ctx context.Context, obj *ent.Group, after *ent.Cursor, first *int, before *ent.Cursor, last *int) (*ent.UserConnection, error) {
	actx := auth.GetAuthContext(ctx)
	ok, err := actx.Viewer.QueryGroups().Where(group.NameIn(obj.Name, "superuser")).Exist(ctx)
	if err != nil {
		return nil, err
	}
	if ok {
		obj.QueryUsers().Paginate(ctx, after, first, before, last)
	}
	return nil, nil
}

func (r *groupResolver) ViewerIsIn(ctx context.Context, obj *ent.Group) (bool, error) {
	actx := auth.GetAuthContext(ctx)
	if actx.Viewer == nil {
		return false, nil
	}
	return obj.QueryUsers().Where(user.ID(actx.Viewer.ID)).Exist(ctx)
}

func (r *mutationResolver) CreateGame(ctx context.Context, input GameInput) (*ent.Game, error) {
	actx := auth.GetAuthContext(ctx)
	if actx.Viewer == nil {
		return nil, nil
	}

	q := mysticism.Client.Game.Create().SetNillableScript(input.Script).SetNillableDescription(input.Description).SetOwner(actx.Viewer).SetNillableScheduledStart(input.ScheduledStart).SetType(enums.Longturn).SetState(enums.Planned)

	if input.Name == nil {
		q.SetName("")
	} else {
		q.SetName(*input.Name)
	}
	if input.Ruleset == nil {
		q.SetRuleset(viper.GetString("civserver.defaultRuleset"))
	} else {
		q.SetRuleset(*input.Ruleset)
	}

	if input.Build == nil {
		q.SetBuild(viper.GetString("civserver.commit"))
	} else {
		q.SetBuild(*input.Build)
	}

	if input.RulesetCommit == nil {
		q.SetRulesetCommit(viper.GetString("civserver.rulesetCommit"))
	} else {
		q.SetRulesetCommit(*input.RulesetCommit)
	}

	if input.Type == nil {
		q.SetType(enums.Longturn)
	} else {
		q.SetType(*input.Type)
	}

	return q.Save(ctx)
}

func (r *mutationResolver) EditGame(ctx context.Context, input GameInput) (*ent.Game, error) {
	if input.ID == nil {
		return nil, fmt.Errorf("no game id")
	}
	g, err := mysticism.Client.Game.Get(ctx, *input.ID)
	if err != nil {
		return nil, err
	}
	actx := auth.GetAuthContext(ctx)

	au := false
	if g.OwnerID != nil && *g.OwnerID == actx.Viewer.ID {
		au = true

	}

	if !au {
		if au, err = actx.Viewer.QueryGroups().Where(group.Name("superuser")).Exist(ctx); err != nil {
			return nil, err
		}
	}

	q := g.Update()
	if input.Script != nil {
		q.SetScript(*input.Script)
	}
	if input.Description != nil {
		q.SetDescription(*input.Description)
	}
	if input.ScheduledStart != nil {
		q.SetScheduledStart(*input.ScheduledStart)
	}
	if input.Name != nil {
		q.SetName(*input.Name)
	}
	if input.Ruleset != nil {
		q.SetRuleset(*input.Ruleset)
	}

	if input.Build != nil {
		q.SetBuild(*input.Build)
	}

	if input.RulesetCommit != nil {
		q.SetRulesetCommit(*input.RulesetCommit)
	}

	if input.Type != nil {
		q.SetType(*input.Type)
	}

	return q.Save(ctx)
}

func (r *mutationResolver) StartGame(ctx context.Context, id int) (*ent.Game, error) {
	g, err := mysticism.Client.Game.Get(ctx, id)
	if err != nil {
		return nil, err
	}
	actx := auth.GetAuthContext(ctx)

	au := false
	if g.OwnerID != nil && *g.OwnerID == actx.Viewer.ID {
		au = true

	}

	if !au {
		if au, err = actx.Viewer.QueryGroups().Where(group.Name("superuser")).Exist(ctx); err != nil {
			return nil, err
		}
	}

	if !au {
		return nil, fmt.Errorf("unauthorized")
	}

	go conscription.Spawn(context.Background(), g)

	return g, err
}

func (r *mutationResolver) JoinGame(ctx context.Context, input *JoinGameInput) (*ent.Game, error) {
	actx := auth.GetAuthContext(ctx)
	if actx.Viewer == nil {
		return nil, fmt.Errorf("no active user session")
	}
	gid := 0
	if input != nil && input.ID != nil {
		gid = *input.ID
	}

	singlePlayer := false
	if input.SinglePlayer != nil {
		singlePlayer = *input.SinglePlayer
	}
	return conscription.JoinGame(ctx, actx.Viewer, gid, singlePlayer)
}

func (r *queryResolver) Identity(ctx context.Context) (*ent.User, error) {
	actx := auth.GetAuthContext(ctx)
	return actx.Viewer, nil
}

func (r *queryResolver) Node(ctx context.Context, id int) (ent.Noder, error) {
	return mysticism.Client.Noder(ctx, id)
}

func (r *queryResolver) Nodes(ctx context.Context, ids []int) ([]ent.Noder, error) {
	return mysticism.Client.Noders(ctx, ids)
}

func (r *queryResolver) Games(ctx context.Context, after *ent.Cursor, first *int, before *ent.Cursor, last *int, filter *ent.GameWhereInput) (*ent.GameConnection, error) {
	if filter == nil {
		return mysticism.Client.Game.Query().Where(game.HasServer()).Paginate(ctx, after, first, before, last)
	}

	p, err := filter.P()
	if err != nil {
		return nil, err
	}
	return mysticism.Client.Game.Query().Where(p).Paginate(ctx, after, first, before, last)
}

func (r *queryResolver) Group(ctx context.Context, id int) (*ent.Group, error) {
	return mysticism.Client.Group.Get(ctx, id)
}

func (r *queryResolver) Groups(ctx context.Context, after *ent.Cursor, first *int, before *ent.Cursor, last *int) (*ent.GroupConnection, error) {
	return mysticism.Client.Group.Query().Paginate(ctx, after, first, before, last)
}

func (r *userResolver) MemberOf(ctx context.Context, obj *ent.User, after *ent.Cursor, first *int, before *ent.Cursor, last *int) (*ent.GroupConnection, error) {
	return obj.QueryGroups().Paginate(ctx, after, first, before, last)
}

func (r *userResolver) EmailAddress(ctx context.Context, obj *ent.User) (*string, error) {
	actx := auth.GetAuthContext(ctx)

	if actx.Viewer.ID == obj.ID {
		return &obj.Email, nil
	}

	ok, err := actx.Viewer.QueryGroups().Where(group.Name("superuser")).Exist(ctx)
	if err != nil {
		return nil, err
	}
	if ok {
		return &obj.Email, nil
	}
	return nil, nil
}

func (r *userResolver) Verification(ctx context.Context, obj *ent.User) (*bool, error) {
	actx := auth.GetAuthContext(ctx)

	if actx.Viewer.ID == obj.ID {
		return &obj.Verified, nil
	}

	ok, err := actx.Viewer.QueryGroups().Where(group.Name("superuser")).Exist(ctx)
	if err != nil {
		return nil, err
	}
	if ok {
		return &obj.Verified, nil
	}
	return nil, nil
}

// Group returns GroupResolver implementation.
func (r *Resolver) Group() GroupResolver { return &groupResolver{r} }

// Mutation returns MutationResolver implementation.
func (r *Resolver) Mutation() MutationResolver { return &mutationResolver{r} }

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

// User returns UserResolver implementation.
func (r *Resolver) User() UserResolver { return &userResolver{r} }

type groupResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type userResolver struct{ *Resolver }
