import type { NextPage } from 'next'
import Link from 'next/link'
import { NextPageContext } from 'next';
import relayOptions from '../lib/relayOptions';
import { getClientEnvironment } from '../lib/client_environment';
import { Button, Spin } from 'antd';
import styles from '../styles/Games.module.css'
import { graphql } from 'relay-runtime';
import type { games_listQuery, games_listQuery$data } from "../lib/queries/__generated__/games_listQuery.graphql";
import { EntryPointComponent, EntryPointContainer, useFragment, useMutation, usePreloadedQuery } from 'react-relay';
import { RelayProps, withRelay } from 'relay-nextjs';
import Modal from 'antd/lib/modal/Modal';
import { Dispatch, SetStateAction, useContext, useEffect, useState } from 'react';
import { CurrentUser, SessionContext } from '../lib/SessionContext';
import { gamesJoinGameMutation } from '../lib/queries/__generated__/gamesJoinGameMutation.graphql';
import { useRouter } from 'next/router';
import { gamesCardFragment$data, gamesCardFragment$key } from '../lib/queries/__generated__/gamesCardFragment.graphql';
import moment from 'moment';

const GAME_FRAGMENT = graphql`
  fragment gamesCardFragment on Game {
    name
    id
    state
    createTime
    description
    type
    ruleset
    scheduledStart
    turn
    players {
      type
      leader
      nation
      number
      user {
        name
      }
    }
}`;
const ROOT_QUERY = graphql`
  query games_listQuery {
    games {
      edges {
        node {
          id
          ...gamesCardFragment
        }
      }
    }
    featuredGames: games(filter: {type: LONGTURN}) {
      edges {
        node {
          id
          ...gamesCardFragment
        }
      }
    }
  }
`;

type GameCardProps = {
  featured?: boolean,
  fragmentRef: any,
}
function GameCard({ fragmentRef , featured} : GameCardProps) {
  const game = useFragment(GAME_FRAGMENT, fragmentRef);
  const players = game.players ?? [];
  const playerLeaders = (players ?? []).map((p: any) => p.leader).filter((p : any) => p);
  const subs = [];
  if (game.description) {
    subs.push(<p style={{fontStyle:"italic", fontSize:"1.5rem"}} key="description">{game.description}</p>);
  }
  if (playerLeaders.length) {
    const slice = playerLeaders.slice(0, 3);
    const others = players.length - slice.length;
    subs.push(<p key="leaders">{slice.join(", ") + ", and " + others + " others"}</p>)
  }
  if (game.state == "RUNNING" && game.turn != null) {
    subs.push(<p key="status">Status: {game.state} (Turn {game.turn})</p>)
  } else if(game.state == "PLANNED" && game.scheduledStart != null) {
    const dur = moment(game.scheduledStart).fromNow();
    subs.push(<p>Starts {dur}</p>);
  } else {
    subs.push(<p key="status">Status: {game.state}</p>)
  }
  subs.push(<p key="ruleset">{game.ruleset}</p>)
  return (
    <li key={game.id} className={featured ? styles.featuredGame : styles.gameCard}>
      <Link href={`/game/${game.id}${game.type == "LONGTURN" ? "/info" : ""}`}>
        <div>
          <div className={featured? styles.featuredName : styles.gameName}>{game.name ?? "Multiplayer Game"}</div>
          {subs}
        </div>
      </Link>
    </li>
  );
}

type Props = {
  showMatchingModal: boolean,
};

type MatchModalProps = {
  visible: boolean,
  setVisible: Dispatch<SetStateAction<boolean>>
  inFlight: boolean,
};

const JOIN_GAME = graphql`
  mutation gamesJoinGameMutation($input: JoinGameInput!) {
    joinGame(input: $input) {
      id
    }
  }
`;

function MatchModal({ visible, inFlight, setVisible }: MatchModalProps) {
  const [single, setSingle] = useState(false);
  const { push } = useRouter();
  const [joinGame, isJoinInFlight] = useMutation<gamesJoinGameMutation>(JOIN_GAME);

  return (<Modal
    visible={visible}
    transitionName=""
    maskTransitionName=""
    footer={null}
    maskStyle={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}
    onCancel={() => { setVisible(false) }}
  >
    <div className={styles.quickHeader}>Quick Start</div>
    <Spin spinning={inFlight}>
      <div className={styles.quick}><Button
        loading={single && isJoinInFlight}
        className={styles.quickBot}
        onClick={() => {
          setSingle(true);
          joinGame({
            variables: { input: { singlePlayer: true } },
            onCompleted: (({ joinGame }, err) => {
              if (joinGame.id) {
                setVisible(false);
                push(`/game/${joinGame.id}`);
              }
            }),
          });
        }}
      >Play vs Bots</Button></div>
      <div className={styles.quick}><Button
        loading={!single && isJoinInFlight}
        className={styles.quickHuman}
        onClick={() => {
          setSingle(false);
          joinGame({
            variables: { input: { singlePlayer: false } },
            onCompleted: (({ joinGame }, err) => {
              if (joinGame.id) {
                setVisible(false);
                push(`/game/${joinGame.id}`);
              }
            }),
          });
        }}
      >Play with Humans</Button></div>
    </Spin>
  </Modal>)
}

function GamesList({ preloadedQuery, showMatchingModal }: RelayProps<Props, games_listQuery>) {
  const [showMatching, setShowMatching] = useState(false);
  const [quickInFlight, setQuickInFlight] = useState(false);

  const { initialSessionFetched, user, setUser, tryMatching, setTryMatching } = useContext(SessionContext)!;

  useEffect(() => {
    if (!initialSessionFetched) {
      return;
    }

    if (tryMatching) {
      setShowMatching(true);
      setTryMatching(false);
    }

    if (!user.name) {
      setQuickInFlight(true);
      (async () => {
        const resp = await fetch('/auth/quick');
        const sess = (await resp.json()) as CurrentUser;
        setUser(sess);
        setQuickInFlight(false);
      })();
    }
  }, [user, initialSessionFetched, tryMatching]);
  const query = usePreloadedQuery(ROOT_QUERY, preloadedQuery);
  const games = query.games;
  return (
    <>
      <MatchModal visible={showMatching} inFlight={quickInFlight} setVisible={setShowMatching} />
      <div className={styles.grid}>
        <div className={styles.public}>
          <h1 className={styles.title}>Public Games</h1>
          <ul className={styles.gameGrid}>
            {games && games.edges?.map((g) => g?.node && <GameCard key={g.node.id} fragmentRef={g.node} />)}
          </ul>
        </div>
        <div className={styles.community}>
          <h1 className={styles.title}>Community</h1>
          <div className={styles.discord}>
            <iframe src="https://discord.com/widget?id=378908274113904641&theme=dark"
              height="500"
              frameBorder={0}
              sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
          </div>
        </div>
      </div>
    </>

  );
}

export default withRelay(GamesList, ROOT_QUERY, relayOptions);
