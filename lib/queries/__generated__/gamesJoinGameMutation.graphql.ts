/**
 * @generated SignedSource<<b6676e780cb5ac1b4abdd3009be74140>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type JoinGameInput = {
  id?: string | null;
  singlePlayer?: boolean | null;
};
export type gamesJoinGameMutation$variables = {
  input: JoinGameInput;
};
export type gamesJoinGameMutation$data = {
  readonly joinGame: {
    readonly id: string;
  };
};
export type gamesJoinGameMutation = {
  variables: gamesJoinGameMutation$variables;
  response: gamesJoinGameMutation$data;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "Game",
    "kind": "LinkedField",
    "name": "joinGame",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "gamesJoinGameMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "gamesJoinGameMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "88e7e16798175c5ba1a1c43d951f02fd",
    "id": null,
    "metadata": {},
    "name": "gamesJoinGameMutation",
    "operationKind": "mutation",
    "text": "mutation gamesJoinGameMutation(\n  $input: JoinGameInput!\n) {\n  joinGame(input: $input) {\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "881ceb8ed642882462ae9355e2fda792";

export default node;
