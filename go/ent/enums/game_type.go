//go:generate go-enum -f=$GOFILE --sqlnullint --names --noprefix --nocase --template marshaler.tmpl

package enums

// GameType x ENUM(
// ShortTurn
// Longturn
// )
type GameType int32

func (GameType) Values() []string {
	return GameTypeNames()
}
