import type { GetServerSideProps, GetStaticProps, NextPage } from 'next'
import Link from 'next/link'
import { NextPageContext } from 'next';
import relayOptions, { relayOptionsAuth } from '../../lib/relayOptions';
import { getClientEnvironment } from '../../lib/client_environment';
import { Button, DatePicker, Form, Input, Select, Spin, Table } from 'antd';
import styles from '../styles/Games.module.css'
import { graphql } from 'relay-runtime';
import type { games_listQuery, games_listQuery$data } from "../../lib/queries/__generated__/games_listQuery.graphql";
import { EntryPointComponent, EntryPointContainer, useMutation, usePreloadedQuery } from 'react-relay';
import { RelayProps, withRelay } from 'relay-nextjs';
import Modal from 'antd/lib/modal/Modal';
import { Dispatch, SetStateAction, useContext, useEffect, useMemo, useState } from 'react';
import { CurrentUser, SessionContext } from '../../lib/SessionContext';
import { gamesJoinGameMutation } from '../../lib/queries/__generated__/gamesJoinGameMutation.graphql';
import { useRouter } from 'next/router';
import { gamesManager_listQuery } from '../../lib/queries/__generated__/gamesManager_listQuery.graphql';
import { useForm } from 'antd/lib/form/Form';
import TextArea from 'antd/lib/input/TextArea';
import { gamesManager_createMutation } from '../../lib/queries/__generated__/gamesManager_createMutation.graphql';

const ROOT_QUERY = graphql`
  query gamesManager_listQuery($filter : GameWhereInput = {typeIn:[SHORTTURN, LONGTURN]}) {
    games(filter: $filter) {
      edges {
        node {
          name
          id
          state
          createTime
          description
          type
          ruleset
          turn
        }
      }
    }
  }
`;

function GamesManager({ preloadedQuery }: RelayProps<{}, gamesManager_listQuery>) {
    const { games } = usePreloadedQuery(ROOT_QUERY, preloadedQuery);

    const dataSource = useMemo(() => {
        return (games?.edges ?? []).map((g) => {
            if (!g?.node) {
                throw ("invalid game");
            }
            return {
                key: g.node.id,
                id: g.node.id,
                name: g.node?.name,
                created: g.node?.createTime,
                state: g.node?.state,
                turn: g.node?.turn,
            };
        })
    }, [games]);

    const columns = [
        {
            title: "ID",
            dataIndex: "id",
            key: "id",
            sorter: (a : {id: string}, b : {id: string}) => (a.id as any) - (b.id as any),
        },
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
        },
        {
            title: "Created",
            dataIndex: "created",
            key: "created",
        },
        {
            title: "State",
            dataIndex: "state",
            key: "state",
        },
        {
            title: "Turn",
            dataIndex: "turn",
            key: "turn",
        },
    ];

    const [showCreate, setShowCreate] = useState(false);
    const [form] = useForm();

    const [createGame, inFlight] = useMutation<gamesManager_createMutation>(graphql`
        mutation gamesManager_createMutation($input: GameInput!) {
            createGame(input: $input) {
                id
            }
        }
    `);

    const { push } = useRouter();
    return (<div>
        <Button onClick={() => { setShowCreate(true) }}>Create Planned Game</Button>
        <Modal
            visible={showCreate}
            confirmLoading={inFlight}
            onCancel={() => { setShowCreate(false) }}
            onOk={() => {
                (async () => {
                    const entry = await form.validateFields();
                    console.log(entry);
                    createGame({
                        variables: { input: { ...entry } },
                        onCompleted: ({ createGame }) => {
                            if (createGame.id) {
                                setShowCreate(false);
                                push(`/manage/games/${createGame.id}`);
                            }
                        }
                    });
                })();
            }}
        >
            <h1>Create Game</h1>
            <Form
                form={form}
                layout="vertical"
                name="login"
                initialValues={{}}
                preserve={false}
            >
                <Form.Item
                    label="Name"
                    name="name"
                    preserve={false}
                    rules={[{ required: true, message: 'Please input game name' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Description"
                    name="description"
                    preserve={false}
                    rules={[{ required: true, message: 'Please input game description' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Ruleset"
                    name="ruleset"
                    preserve={false}
                    rules={[{ required: true, message: 'Please input ruleset' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Scheduled Start"
                    name="scheduledStart"
                    preserve={false}
                >
                    <DatePicker showTime={true} />
                </Form.Item>
                <Form.Item
                    label="Server script"
                    name="script"
                    preserve={false}
                >
                    <TextArea size="large"/>
                </Form.Item>
            </Form>
        </Modal>
        <Table
            dataSource={dataSource}
            columns={columns}
        />
    </div>);
}

export default withRelay(GamesManager, ROOT_QUERY, relayOptionsAuth);
