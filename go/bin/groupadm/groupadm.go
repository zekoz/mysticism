package main

import (
	"context"
	"flag"
	"log"
	"mysticism"
	"mysticism/ent"
	"mysticism/ent/migrate"

	"github.com/docker/docker/client"
	"github.com/golang/glog"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("mysticism")
	viper.AddConfigPath("$HOME/.mysticism")
	viper.AddConfigPath(".")
	viper.SetDefault("civsever.repo", "https://github.com/longturn/freeciv21")
	viper.SetDefault("civserver.setupCommands", []string{"set scorelog enabled",
		"metaserver http://localhost:8888/meta",
		"metaconnection up",
		"set autosaves timer",
	})
}

func main() {
	flag.Parse()
	if err := viper.ReadInConfig(); err != nil {
		glog.Fatal(err)
	}
	viper.WatchConfig()
	ctx := context.Background()
	c, err := client.NewClientWithOpts(client.WithHost("unix:///run/fcweb/podman.sock"))
	if err != nil {
		log.Fatal(err)
	}
	mysticism.Docker = c

	client, err := ent.Open("postgres", "user=fcweb dbname=fcweb host=/var/run/postgresql")
	if err != nil {
		log.Fatal(err)
	}

	mysticism.Client = client.Debug()
	if err := mysticism.Client.Schema.Create(ctx, migrate.WithGlobalUniqueID(true)); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	mysticism.Client.User.UpdateOneID(61).AddGroupIDs(17179869184, 17179869185).ExecX(ctx)

	/*g, err := mysticism.Client.Group.Create().SetName("gamemaster").SetDescription("User that can manage games").AddUserIDs(1, 2, 3, 4, 5, 48).Save(ctx)
	if err != nil {
		glog.Fatal(err)
	}
	glog.Infof("created group %v", g)*/
}
