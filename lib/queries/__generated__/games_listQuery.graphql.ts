/**
 * @generated SignedSource<<9cd49424ca008567499a4cb7cf489096>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type games_listQuery$variables = {};
export type games_listQuery$data = {
  readonly games: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly id: string;
        readonly " $fragmentSpreads": FragmentRefs<"gamesCardFragment">;
      } | null;
    } | null> | null;
  } | null;
  readonly featuredGames: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly id: string;
        readonly " $fragmentSpreads": FragmentRefs<"gamesCardFragment">;
      } | null;
    } | null> | null;
  } | null;
};
export type games_listQuery = {
  variables: games_listQuery$variables;
  response: games_listQuery$data;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "GameEdge",
    "kind": "LinkedField",
    "name": "edges",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Game",
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "gamesCardFragment"
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
],
v2 = [
  {
    "kind": "Literal",
    "name": "filter",
    "value": {
      "type": "LONGTURN"
    }
  }
],
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v5 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "GameEdge",
    "kind": "LinkedField",
    "name": "edges",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Game",
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          (v3/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "state",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "createTime",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "description",
            "storageKey": null
          },
          (v4/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "ruleset",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "scheduledStart",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "turn",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Player",
            "kind": "LinkedField",
            "name": "players",
            "plural": true,
            "selections": [
              (v4/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "leader",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "nation",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "number",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "User",
                "kind": "LinkedField",
                "name": "user",
                "plural": false,
                "selections": [
                  (v3/*: any*/),
                  (v0/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "games_listQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "GameConnection",
        "kind": "LinkedField",
        "name": "games",
        "plural": false,
        "selections": (v1/*: any*/),
        "storageKey": null
      },
      {
        "alias": "featuredGames",
        "args": (v2/*: any*/),
        "concreteType": "GameConnection",
        "kind": "LinkedField",
        "name": "games",
        "plural": false,
        "selections": (v1/*: any*/),
        "storageKey": "games(filter:{\"type\":\"LONGTURN\"})"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "games_listQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "GameConnection",
        "kind": "LinkedField",
        "name": "games",
        "plural": false,
        "selections": (v5/*: any*/),
        "storageKey": null
      },
      {
        "alias": "featuredGames",
        "args": (v2/*: any*/),
        "concreteType": "GameConnection",
        "kind": "LinkedField",
        "name": "games",
        "plural": false,
        "selections": (v5/*: any*/),
        "storageKey": "games(filter:{\"type\":\"LONGTURN\"})"
      }
    ]
  },
  "params": {
    "cacheID": "885fde992041f32559590383fd97e292",
    "id": null,
    "metadata": {},
    "name": "games_listQuery",
    "operationKind": "query",
    "text": "query games_listQuery {\n  games {\n    edges {\n      node {\n        id\n        ...gamesCardFragment\n      }\n    }\n  }\n  featuredGames: games(filter: {type: LONGTURN}) {\n    edges {\n      node {\n        id\n        ...gamesCardFragment\n      }\n    }\n  }\n}\n\nfragment gamesCardFragment on Game {\n  name\n  id\n  state\n  createTime\n  description\n  type\n  ruleset\n  scheduledStart\n  turn\n  players {\n    type\n    leader\n    nation\n    number\n    user {\n      name\n      id\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "456965c9ce05f8ab0ad6a7e024d74c25";

export default node;
