package websocket

import (
	"fmt"
	"mysticism"
	"mysticism/ent/game"
	"net"
	"net/http"
	"strconv"
	"strings"

	"github.com/golang/glog"
	"github.com/gorilla/websocket"
)

const bufferSize = 65536

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  bufferSize,
		WriteBufferSize: bufferSize,
		CheckOrigin:     func(r *http.Request) bool { return true },
		Subprotocols:    []string{"binary"},
	}
)

func Handler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	glog.Infof("received connection to %s", r.URL)
	ss := strings.Split(r.URL.Path, "/")
	id, err := strconv.Atoi(ss[len(ss)-1])
	if err != nil {
		glog.Errorf("invalid url %s", r.URL)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	g, err := mysticism.Client.Game.Query().Where(game.ID(id)).WithServer().Only(ctx)
	if err != nil {
		glog.Errorf("game not found %d", id)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if len(g.Edges.Server) != 1 {
		glog.Errorf("server not found for game %d", id)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	s := g.Edges.Server[0]

	fc, err := net.Dial("tcp", fmt.Sprintf("localhost:%d", s.Port))
	if err != nil {
		glog.Errorf("failed to connect to fc server %d: %s", s.Port, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		glog.Errorf("failed to upgrade ws %s", err)
		return
	}

	go proxy(fc, ws)
}

func proxy(fc net.Conn, ws *websocket.Conn) {
	go func() {
		defer func() {
			ws.Close()
			fc.Close()
		}()
		buffer := make([]byte, bufferSize)
		for {
			n, err := fc.Read(buffer)
			if err != nil {
				glog.Warningf("failed to read: %s", err)
				return
			}
			if err := ws.WriteMessage(websocket.BinaryMessage, buffer[0:n]); err != nil {
				glog.Warningf("failed to write: %s", err)
				return
			}
		}
	}()
	go func() {
		defer func() {
			ws.Close()
			fc.Close()
		}()
		for {
			_, bs, err := ws.ReadMessage()
			if err != nil {
				glog.Warningf("failed to read: %s", err)
				return
			}
			if _, err := fc.Write(bs); err != nil {
				glog.Warningf("failed to write: %s", err)
				return
			}
		}
	}()
}
