/**
 * @generated SignedSource<<80bdde8138be4c3b7346cd6393d9496e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type GameState = "PLANNED" | "PREGAME" | "RUNNING" | "ENDED" | "COMPLETE" | "SUSPENDED" | "%future added value";
export type GameType = "SHORTTURN" | "LONGTURN" | "%future added value";
export type GameWhereInput = {
  not?: GameWhereInput | null;
  and?: ReadonlyArray<GameWhereInput> | null;
  or?: ReadonlyArray<GameWhereInput> | null;
  createTime?: any | null;
  createTimeNEQ?: any | null;
  createTimeIn?: ReadonlyArray<any> | null;
  createTimeNotIn?: ReadonlyArray<any> | null;
  createTimeGT?: any | null;
  createTimeGTE?: any | null;
  createTimeLT?: any | null;
  createTimeLTE?: any | null;
  updateTime?: any | null;
  updateTimeNEQ?: any | null;
  updateTimeIn?: ReadonlyArray<any> | null;
  updateTimeNotIn?: ReadonlyArray<any> | null;
  updateTimeGT?: any | null;
  updateTimeGTE?: any | null;
  updateTimeLT?: any | null;
  updateTimeLTE?: any | null;
  name?: string | null;
  nameNEQ?: string | null;
  nameIn?: ReadonlyArray<string> | null;
  nameNotIn?: ReadonlyArray<string> | null;
  nameGT?: string | null;
  nameGTE?: string | null;
  nameLT?: string | null;
  nameLTE?: string | null;
  nameContains?: string | null;
  nameHasPrefix?: string | null;
  nameHasSuffix?: string | null;
  nameIsNil?: boolean | null;
  nameNotNil?: boolean | null;
  nameEqualFold?: string | null;
  nameContainsFold?: string | null;
  state?: GameState | null;
  stateNEQ?: GameState | null;
  stateIn?: ReadonlyArray<GameState> | null;
  stateNotIn?: ReadonlyArray<GameState> | null;
  type?: GameType | null;
  typeNEQ?: GameType | null;
  typeIn?: ReadonlyArray<GameType> | null;
  typeNotIn?: ReadonlyArray<GameType> | null;
  description?: string | null;
  descriptionNEQ?: string | null;
  descriptionIn?: ReadonlyArray<string> | null;
  descriptionNotIn?: ReadonlyArray<string> | null;
  descriptionGT?: string | null;
  descriptionGTE?: string | null;
  descriptionLT?: string | null;
  descriptionLTE?: string | null;
  descriptionContains?: string | null;
  descriptionHasPrefix?: string | null;
  descriptionHasSuffix?: string | null;
  descriptionIsNil?: boolean | null;
  descriptionNotNil?: boolean | null;
  descriptionEqualFold?: string | null;
  descriptionContainsFold?: string | null;
  build?: string | null;
  buildNEQ?: string | null;
  buildIn?: ReadonlyArray<string> | null;
  buildNotIn?: ReadonlyArray<string> | null;
  buildGT?: string | null;
  buildGTE?: string | null;
  buildLT?: string | null;
  buildLTE?: string | null;
  buildContains?: string | null;
  buildHasPrefix?: string | null;
  buildHasSuffix?: string | null;
  buildEqualFold?: string | null;
  buildContainsFold?: string | null;
  ruleset?: string | null;
  rulesetNEQ?: string | null;
  rulesetIn?: ReadonlyArray<string> | null;
  rulesetNotIn?: ReadonlyArray<string> | null;
  rulesetGT?: string | null;
  rulesetGTE?: string | null;
  rulesetLT?: string | null;
  rulesetLTE?: string | null;
  rulesetContains?: string | null;
  rulesetHasPrefix?: string | null;
  rulesetHasSuffix?: string | null;
  rulesetEqualFold?: string | null;
  rulesetContainsFold?: string | null;
  rulesetCommit?: string | null;
  rulesetCommitNEQ?: string | null;
  rulesetCommitIn?: ReadonlyArray<string> | null;
  rulesetCommitNotIn?: ReadonlyArray<string> | null;
  rulesetCommitGT?: string | null;
  rulesetCommitGTE?: string | null;
  rulesetCommitLT?: string | null;
  rulesetCommitLTE?: string | null;
  rulesetCommitContains?: string | null;
  rulesetCommitHasPrefix?: string | null;
  rulesetCommitHasSuffix?: string | null;
  rulesetCommitEqualFold?: string | null;
  rulesetCommitContainsFold?: string | null;
  id?: string | null;
  idNEQ?: string | null;
  idIn?: ReadonlyArray<string> | null;
  idNotIn?: ReadonlyArray<string> | null;
  idGT?: string | null;
  idGTE?: string | null;
  idLT?: string | null;
  idLTE?: string | null;
  hasPlayers?: boolean | null;
  hasPlayersWith?: ReadonlyArray<PlayerWhereInput> | null;
  hasServer?: boolean | null;
  hasServerWith?: ReadonlyArray<ServerWhereInput> | null;
};
export type PlayerWhereInput = {
  not?: PlayerWhereInput | null;
  and?: ReadonlyArray<PlayerWhereInput> | null;
  or?: ReadonlyArray<PlayerWhereInput> | null;
  userID?: string | null;
  userIDNEQ?: string | null;
  userIDIn?: ReadonlyArray<string> | null;
  userIDNotIn?: ReadonlyArray<string> | null;
  userIDIsNil?: boolean | null;
  userIDNotNil?: boolean | null;
  number?: number | null;
  numberNEQ?: number | null;
  numberIn?: ReadonlyArray<number> | null;
  numberNotIn?: ReadonlyArray<number> | null;
  numberGT?: number | null;
  numberGTE?: number | null;
  numberLT?: number | null;
  numberLTE?: number | null;
  leader?: string | null;
  leaderNEQ?: string | null;
  leaderIn?: ReadonlyArray<string> | null;
  leaderNotIn?: ReadonlyArray<string> | null;
  leaderGT?: string | null;
  leaderGTE?: string | null;
  leaderLT?: string | null;
  leaderLTE?: string | null;
  leaderContains?: string | null;
  leaderHasPrefix?: string | null;
  leaderHasSuffix?: string | null;
  leaderEqualFold?: string | null;
  leaderContainsFold?: string | null;
  nation?: string | null;
  nationNEQ?: string | null;
  nationIn?: ReadonlyArray<string> | null;
  nationNotIn?: ReadonlyArray<string> | null;
  nationGT?: string | null;
  nationGTE?: string | null;
  nationLT?: string | null;
  nationLTE?: string | null;
  nationContains?: string | null;
  nationHasPrefix?: string | null;
  nationHasSuffix?: string | null;
  nationEqualFold?: string | null;
  nationContainsFold?: string | null;
  type?: string | null;
  typeNEQ?: string | null;
  typeIn?: ReadonlyArray<string> | null;
  typeNotIn?: ReadonlyArray<string> | null;
  typeGT?: string | null;
  typeGTE?: string | null;
  typeLT?: string | null;
  typeLTE?: string | null;
  typeContains?: string | null;
  typeHasPrefix?: string | null;
  typeHasSuffix?: string | null;
  typeEqualFold?: string | null;
  typeContainsFold?: string | null;
  gameID?: string | null;
  gameIDNEQ?: string | null;
  gameIDIn?: ReadonlyArray<string> | null;
  gameIDNotIn?: ReadonlyArray<string> | null;
  id?: string | null;
  idNEQ?: string | null;
  idIn?: ReadonlyArray<string> | null;
  idNotIn?: ReadonlyArray<string> | null;
  idGT?: string | null;
  idGTE?: string | null;
  idLT?: string | null;
  idLTE?: string | null;
  hasGame?: boolean | null;
  hasGameWith?: ReadonlyArray<GameWhereInput> | null;
  hasUser?: boolean | null;
  hasUserWith?: ReadonlyArray<UserWhereInput> | null;
};
export type UserWhereInput = {
  not?: UserWhereInput | null;
  and?: ReadonlyArray<UserWhereInput> | null;
  or?: ReadonlyArray<UserWhereInput> | null;
  createTime?: any | null;
  createTimeNEQ?: any | null;
  createTimeIn?: ReadonlyArray<any> | null;
  createTimeNotIn?: ReadonlyArray<any> | null;
  createTimeGT?: any | null;
  createTimeGTE?: any | null;
  createTimeLT?: any | null;
  createTimeLTE?: any | null;
  updateTime?: any | null;
  updateTimeNEQ?: any | null;
  updateTimeIn?: ReadonlyArray<any> | null;
  updateTimeNotIn?: ReadonlyArray<any> | null;
  updateTimeGT?: any | null;
  updateTimeGTE?: any | null;
  updateTimeLT?: any | null;
  updateTimeLTE?: any | null;
  name?: string | null;
  nameNEQ?: string | null;
  nameIn?: ReadonlyArray<string> | null;
  nameNotIn?: ReadonlyArray<string> | null;
  nameGT?: string | null;
  nameGTE?: string | null;
  nameLT?: string | null;
  nameLTE?: string | null;
  nameContains?: string | null;
  nameHasPrefix?: string | null;
  nameHasSuffix?: string | null;
  nameEqualFold?: string | null;
  nameContainsFold?: string | null;
  email?: string | null;
  emailNEQ?: string | null;
  emailIn?: ReadonlyArray<string> | null;
  emailNotIn?: ReadonlyArray<string> | null;
  emailGT?: string | null;
  emailGTE?: string | null;
  emailLT?: string | null;
  emailLTE?: string | null;
  emailContains?: string | null;
  emailHasPrefix?: string | null;
  emailHasSuffix?: string | null;
  emailEqualFold?: string | null;
  emailContainsFold?: string | null;
  verified?: boolean | null;
  verifiedNEQ?: boolean | null;
  active?: boolean | null;
  activeNEQ?: boolean | null;
  id?: string | null;
  idNEQ?: string | null;
  idIn?: ReadonlyArray<string> | null;
  idNotIn?: ReadonlyArray<string> | null;
  idGT?: string | null;
  idGTE?: string | null;
  idLT?: string | null;
  idLTE?: string | null;
  hasPlayer?: boolean | null;
  hasPlayerWith?: ReadonlyArray<PlayerWhereInput> | null;
};
export type ServerWhereInput = {
  not?: ServerWhereInput | null;
  and?: ReadonlyArray<ServerWhereInput> | null;
  or?: ReadonlyArray<ServerWhereInput> | null;
  host?: string | null;
  hostNEQ?: string | null;
  hostIn?: ReadonlyArray<string> | null;
  hostNotIn?: ReadonlyArray<string> | null;
  hostGT?: string | null;
  hostGTE?: string | null;
  hostLT?: string | null;
  hostLTE?: string | null;
  hostContains?: string | null;
  hostHasPrefix?: string | null;
  hostHasSuffix?: string | null;
  hostIsNil?: boolean | null;
  hostNotNil?: boolean | null;
  hostEqualFold?: string | null;
  hostContainsFold?: string | null;
  port?: number | null;
  portNEQ?: number | null;
  portIn?: ReadonlyArray<number> | null;
  portNotIn?: ReadonlyArray<number> | null;
  portGT?: number | null;
  portGTE?: number | null;
  portLT?: number | null;
  portLTE?: number | null;
  containerID?: string | null;
  containerIDNEQ?: string | null;
  containerIDIn?: ReadonlyArray<string> | null;
  containerIDNotIn?: ReadonlyArray<string> | null;
  containerIDGT?: string | null;
  containerIDGTE?: string | null;
  containerIDLT?: string | null;
  containerIDLTE?: string | null;
  containerIDContains?: string | null;
  containerIDHasPrefix?: string | null;
  containerIDHasSuffix?: string | null;
  containerIDEqualFold?: string | null;
  containerIDContainsFold?: string | null;
  id?: string | null;
  idNEQ?: string | null;
  idIn?: ReadonlyArray<string> | null;
  idNotIn?: ReadonlyArray<string> | null;
  idGT?: string | null;
  idGTE?: string | null;
  idLT?: string | null;
  idLTE?: string | null;
  hasGame?: boolean | null;
  hasGameWith?: ReadonlyArray<GameWhereInput> | null;
};
export type gamesManager_listQuery$variables = {
  filter?: GameWhereInput | null;
};
export type gamesManager_listQuery$data = {
  readonly games: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly name: string | null;
        readonly id: string;
        readonly state: GameState;
        readonly createTime: any | null;
        readonly description: string | null;
        readonly type: GameType;
        readonly ruleset: string;
        readonly turn: number | null;
      } | null;
    } | null> | null;
  } | null;
};
export type gamesManager_listQuery = {
  variables: gamesManager_listQuery$variables;
  response: gamesManager_listQuery$data;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": {
      "typeIn": [
        "SHORTTURN",
        "LONGTURN"
      ]
    },
    "kind": "LocalArgument",
    "name": "filter"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "filter",
        "variableName": "filter"
      }
    ],
    "concreteType": "GameConnection",
    "kind": "LinkedField",
    "name": "games",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "GameEdge",
        "kind": "LinkedField",
        "name": "edges",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Game",
            "kind": "LinkedField",
            "name": "node",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "state",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createTime",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "type",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "ruleset",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "turn",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "gamesManager_listQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "gamesManager_listQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "fdc85350261299b7403885b874b32b50",
    "id": null,
    "metadata": {},
    "name": "gamesManager_listQuery",
    "operationKind": "query",
    "text": "query gamesManager_listQuery(\n  $filter: GameWhereInput = {typeIn: [SHORTTURN, LONGTURN]}\n) {\n  games(filter: $filter) {\n    edges {\n      node {\n        name\n        id\n        state\n        createTime\n        description\n        type\n        ruleset\n        turn\n      }\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "ae8d544a2ee07ab78502399d1288e148";

export default node;
