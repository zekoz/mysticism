package auth

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	mrand "math/rand"
	"mysticism"
	"mysticism/ent"
	"mysticism/ent/user"
	"net/http"
	"net/url"
	"strings"
	"time"
	"unicode"

	"github.com/golang/glog"
	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
)

type sessionKey struct{}

const sessionExtension = "session"

type AuthContext struct {
	Viewer *ent.User
}

func WithAuthContext(ctx context.Context, ac *AuthContext) context.Context {
	return context.WithValue(ctx, sessionKey{}, ac)
}

func GetAuthContext(ctx context.Context) *AuthContext {
	if val, ok := ctx.Value(sessionKey{}).(*AuthContext); ok && val != nil {
		return val
	}
	panic("missing auth context")
}

// ViewerContext finds the user from the context.
func ViewerContext(ctx context.Context, client *ent.Client) *ent.User {
	return GetAuthContext(ctx).Viewer
}

func randomSessionID() (string, error) {
	bs := make([]byte, 30)
	n, err := rand.Read(bs)
	if err != nil {
		return "", err
	}
	if n != 30 {
		return "", fmt.Errorf("internal error")
	}

	return base64.RawURLEncoding.EncodeToString(bs), nil
}

type LoginParams struct {
	Username     string `json:"username,omitempty"`
	Password     string `json:"password,omitempty"`
	Email        string `json:"email,omitempty"`
	Verification string `json:"verification,omitempty"`
}

type SessionResponse struct {
	Name string `json:"name,omitempty"`
	ID   int    `json:"id,omitempty"`
}

type SessionVal struct {
	ID int `json:"id,omitempty"`
}

func redisSave(ctx context.Context, w http.ResponseWriter, sid string, uid int) error {
	age := 5000 * time.Hour
	bs, err := json.Marshal(SessionVal{ID: uid})
	if err != nil {
		return err
	}
	if err := mysticism.Redis.SetEX(ctx, "sess_"+sid, bs, age).Err(); err != nil {
		return err
	}
	http.SetCookie(w, &http.Cookie{Name: cookieName, Path: "/", Value: sid, MaxAge: int(age.Seconds())})
	return nil
}

func redisDelete(ctx context.Context, w http.ResponseWriter, sid string) error {
	if err := mysticism.Redis.Del(ctx, "sess_"+sid).Err(); err != nil {
		return err
	}
	http.SetCookie(w, &http.Cookie{Name: cookieName, Path: "/", MaxAge: -1})
	return nil
}

func redisGet(r *http.Request) (int, error) {
	c, err := r.Cookie(cookieName)
	if err != nil {
		return 0, err
	}
	val, err := mysticism.Redis.Get(r.Context(), "sess_"+c.Value).Result()
	if err != nil {
		return 0, err
	}
	var v SessionVal
	if err := json.Unmarshal([]byte(val), &v); err != nil {
		return 0, err
	}
	return v.ID, nil
}

func Quick(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var uname string
	for i := 0; i < 5; i++ {
		choices := viper.GetStringSlice("provisionalNames")
		choice := choices[mrand.Intn(len(choices))]

		num := mrand.Intn(9999) + 1
		uname = fmt.Sprintf("%s~%04d", choice, num)
		_, err := mysticism.Client.User.Query().Where(user.NameEqualFold(uname)).FirstID(ctx)
		if _, ok := err.(*ent.NotFoundError); ok {
			break
		}
	}

	hash, err := bcrypt.GenerateFromPassword([]byte("ifSomeonguessd this iGEus they deserve it"), 0)
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}

	u, err := mysticism.Client.User.Create().SetName(uname).SetVerified(false).SetPassword(hash).SetEmail("provisional@example.com").SetProvisional(false).Save(ctx)
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}
	sid, err := randomSessionID()
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}
	if err := redisSave(ctx, w, sid, u.ID); err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}
	w.WriteHeader(http.StatusOK)
	bs, err := json.Marshal(SessionResponse{
		Name: u.Name,
		ID:   u.ID,
	})
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}

	w.Write(bs)
	return
}

func Login(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var val LoginParams
	dec := json.NewDecoder(r.Body)
	if err := dec.Decode(&val); err != nil {
		glog.V(2).Info(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request."))
		return
	}

	u, err := mysticism.Client.User.Query().Where(user.NameEqualFold(val.Username)).Only(ctx)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("Your username or password is incorrect."))
		return
	}
	if err := bcrypt.CompareHashAndPassword(u.Password, []byte(val.Password)); err != nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("Your username or password is incorrect."))
		return
	}
	if !u.Active {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("Your account is not active."))
		return
	}

	sid, err := randomSessionID()
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}

	if err := redisSave(ctx, w, sid, u.ID); err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}
	bs, err := json.Marshal(SessionResponse{
		Name: u.Name,
		ID:   u.ID,
	})
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(bs)
	return
}

func verifyCaptcha(verification string) error {
	hcd := url.Values{}
	hcd.Set("response", verification)
	hcd.Set("secret", "0xAC8e15aC01E1f435E9044d688f5135Ae74312175")
	r, err := http.NewRequest("POST", "https://hcaptcha.com/siteverify", strings.NewReader(hcd.Encode()))
	if err != nil {
		return err
	}
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	res, err := http.DefaultClient.Do(r)
	if err != nil {
		glog.Error(err)
		return err
	}
	js := struct {
		Success bool `json:"success"`
	}{}
	if err := json.NewDecoder(res.Body).Decode(&js); err != nil {
		glog.Error(err)
	}
	if !js.Success {
		return fmt.Errorf("failed captcha")
	}
	return nil
}

func validateName(name string) bool {
	for _, c := range name {
		if unicode.IsLetter(c) {
			continue
		}
		if unicode.IsNumber(c) {
			continue
		}
		return false
	}

	return true
}

func Register(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var val LoginParams
	dec := json.NewDecoder(r.Body)
	if err := dec.Decode(&val); err != nil {
		glog.V(2).Info(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request."))
		return
	}

	if err := verifyCaptcha(val.Verification); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid captcha."))
		return
	}

	if !validateName(val.Username) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Cannot register this username."))
		return
	}

	u, err := mysticism.Client.User.Query().Where(user.NameEqualFold(val.Username)).Only(ctx)
	if _, ok := err.(*ent.NotFoundError); !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Cannot register this username."))
		return
	} else if !ok && err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}
	if val.Email == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Email address required."))
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(val.Password), 0)
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}

	// If the current session is a provisional user, re-use that user.
	uid, err := redisGet(r)
	if err != nil {
		glog.V(1).Info(err)
	}
	if err == nil {
		u, err = mysticism.Client.User.Get(r.Context(), uid)
		if err != nil {
			glog.Error(err)
		}
	}

	if u != nil {
		u, err = mysticism.Client.User.UpdateOne(u).SetName(val.Username).SetPassword(hash).SetEmail(val.Email).SetProvisional(false).Save(ctx)
	} else {
		u, err = mysticism.Client.User.Create().SetName(val.Username).SetPassword(hash).SetEmail(val.Email).SetVerified(false).Save(ctx)
	}

	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}
	if u == nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}

	sid, err := randomSessionID()
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}

	go func() {
		if err := ConfirmEmail(u); err != nil {
			glog.Error(err)
		}
	}()

	if err := redisSave(ctx, w, sid, u.ID); err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}
	w.WriteHeader(http.StatusOK)
	bs, err := json.Marshal(SessionResponse{
		Name: u.Name,
		ID:   u.ID,
	})
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}

	w.Write(bs)
	return
}

func Logout(w http.ResponseWriter, r *http.Request) {
	c, err := r.Cookie(cookieName)
	if err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}
	if err := redisDelete(r.Context(), w, c.Value); err != nil {
		glog.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Try again later."))
		return
	}
}

func Session(w http.ResponseWriter, r *http.Request) {
	if strings.HasPrefix(r.URL.RawQuery, "id=") {
		glog.V(2).Info("received request: %s", r.URL)
		r.AddCookie(&http.Cookie{Name: cookieName, Path: "/", Value: strings.TrimPrefix(r.URL.RawQuery, "id=")})
	}
	var val SessionResponse
	uid, err := redisGet(r)
	if err != nil {
		glog.V(1).Info(err)
	}
	if err == nil {
		val.ID = uid
		u, err := mysticism.Client.User.Get(r.Context(), uid)
		if err != nil {
			glog.Error(err)
		}
		val.Name = u.Name
	}

	if err := json.NewEncoder(w).Encode(val); err != nil {
		glog.Error(err)
	}
}
