package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Player holds the schema definition for the Player entity.
type Player struct {
	ent.Schema
}

// Fields of the Player.
func (Player) Fields() []ent.Field {
	return []ent.Field{
		field.Int("user_id").Nillable().Optional(),
		field.Int("number").Immutable(),
		field.String("leader"),
		field.String("nation"),
		field.String("type"),
		field.Int("game_id"),
	}
}

// Edges of the Player.
func (Player) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("game", Game.Type).Ref("players").Field("game_id").Required().Unique(),
		edge.To("user", User.Type).Field("user_id").Unique(),
	}
}

// Indexes of the Player.
func (Player) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("game_id", "user_id").Unique(),
		index.Fields("user_id"),
	}
}
