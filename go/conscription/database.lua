local request = require "http.request"
local rapidjson = require "rapidjson"

function database_init()
end

function database_free()
end

function user_log()
end

function user_exists(conn)
    return true
end

function user_verify(conn, plaintext)
    local req = request.new_from_uri("http://localhost:8888/auth/session?id="..plaintext)
    local hdrs, stream = req:go()
    local body = assert(stream:get_body_as_string())
    local sess = assert(rapidjson.decode(body))
    if sess.name and sess.name == auth.get_username(conn) then
        return true
    else
        return false
    end
end
