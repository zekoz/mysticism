package main

import (
	"context"
	"flag"
	"log"
	"mysticism"
	"mysticism/conscription"
	"mysticism/ent"
	"mysticism/ent/enums"
	"mysticism/ent/migrate"

	"github.com/docker/docker/client"
	"github.com/golang/glog"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("mysticism")
	viper.AddConfigPath("$HOME/.mysticism")
	viper.AddConfigPath(".")
	viper.SetDefault("civsever.repo", "https://github.com/longturn/freeciv21")
	viper.SetDefault("civserver.setupCommands", []string{"set scorelog enabled",
		"metaserver http://localhost:8888/meta",
		"metaconnection up",
		"set autosaves timer",
	})
}

func main() {
	flag.Parse()
	if err := viper.ReadInConfig(); err != nil {
		glog.Fatal(err)
	}
	viper.WatchConfig()
	ctx := context.Background()
	c, err := client.NewClientWithOpts(client.WithHost("unix:///run/fcweb/podman.sock"))
	if err != nil {
		log.Fatal(err)
	}
	mysticism.Docker = c

	client, err := ent.Open("postgres", "user=fcweb dbname=fcweb host=/var/run/postgresql")
	if err != nil {
		log.Fatal(err)
	}

	mysticism.Client = client.Debug()
	if err := mysticism.Client.Schema.Create(ctx, migrate.WithGlobalUniqueID(true)); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	r := viper.GetString("civserver.commit")
	rr := viper.GetString("civserver.rulesetCommit")
	g, err := mysticism.Client.Game.Create().SetBuild(r).SetType(enums.ShortTurn).SetState(enums.Pregame).SetRuleset("royale").SetRulesetCommit(rr).Save(ctx)
	if err != nil {
		glog.Fatal(err)
	}

	conscription.Spawn(ctx, g)
}
