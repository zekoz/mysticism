//go:generate go-enum -f=$GOFILE --sqlnullint --names --noprefix --nocase --template marshaler.tmpl

package enums

// GameState x ENUM(
// Planned
// Pregame
// Running
// Ended
// Complete
// Suspended
// )
type GameState int32

func (GameState) Values() []string {
	return GameStateNames()
}
