package herodotus

import (
	"context"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/golang/glog"
	"github.com/hanwen/go-fuse/v2/fs"
	"github.com/hanwen/go-fuse/v2/fuse"
)

var (
	_ = (fs.InodeEmbedder)((*Root)(nil))
	_ = (fs.NodeLookuper)((*Root)(nil))
)

type TurnScore struct{
	Turn int
	Data map[int]int
}

type Root struct {
	fs.Inode
	realRootPath string
	ch chan<- TurnScore
}

func (r *Root) Lookup(ctx context.Context, name string, out *fuse.EntryOut) (*fs.Inode, syscall.Errno) {
	ps := strings.Split(name, ".")
	if ps[len(ps)-1] == "log" {
		return r.NewPersistentInode(ctx, &ProxyFile{realPath: filepath.Join(r.realRootPath, name), ch: ch}, fs.StableAttr{Mode:fuse.S_IFREG}), 0
	}

	return r.NewPersistentInode(ctx, &ProxyFile{realPath: filepath.Join(r.realRootPath, name)}, fs.StableAttr{Mode:fuse.S_IFREG}), 0
}

type ProxyFile struct {
	fs.Inode
	realPath string

	ch chan<- TurnScore

}

var (
	_ = (fs.InodeEmbedder)((*ProxyFile)(nil))
	_ = (fs.NodeGetattrer)((*ProxyFile)(nil))
	_ = (fs.NodeOpener)((*ProxyFile)(nil))
	_ = (fs.NodeCreater)((*ProxyFile)(nil))
)

func (f *ProxyFile) Getattr(ctx context.Context, f fs.FileHandle, out *fuse.AttrOut) syscall.Errno {
	var st syscall.Stat_t
	err := syscall.Stat(f.realPath, &st).(syscall.Errno)
	out.FromStat(st)
	return err
}

func (f *ProxyFile) Open(ctx context.Context, flags uint32) (fs.FileHandle, uint32, syscall.Errno) {
	os.OpenFile(f.realPath, int(flags))

	if o.data == nil {
		rc, err := o.Reader()
		if err != nil {
			return nil, 0, syscall.EIO
		}
		bs, err := ioutil.ReadAll(rc)
		if err != nil {
			return nil, 0, syscall.EIO
		}

		o.data = bs
	}
	return nil, fuse.FOPEN_KEEP_CACHE, fs.OK
}

// Read simply returns the data that was already unpacked in the Open call
func (o *File) Read(ctx context.Context, f fs.FileHandle, dest []byte, off int64) (fuse.ReadResult, syscall.Errno) {
	end := int(off) + len(dest)
	if end > len(o.data) {
		end = len(o.data)
	}
	return fuse.ReadResultData(o.data[off:end]), fs.OK
}

type Dir struct {
	fs.Inode
	tree *object.Tree
}

func (d *Dir) OnAdd(ctx context.Context) {
	it := d.tree.Files()
	for {
		f, err := it.Next()
		if err != nil {
			break
		}

		ch := d.NewPersistentInode(ctx, &File{File: f}, fs.StableAttr{})
		d.AddChild(f.Name, ch, true)
	}
}

type repo struct {
	git.Repository
}

func (r *repo) revision(ctx context.Context, rev string) (*object.Commit, error) {
	// First try commit hash.
	commit, err := r.CommitObject(plumbing.NewHash(rev))
	if err == nil && commit != nil {
		return commit, nil
	}

	// Fetch references from upstream, because they might have changed.
	if err := r.FetchContext(ctx, &git.FetchOptions{
		RefSpecs: []config.RefSpec{"+refs/heads/*:refs/heads/*"},
	}); err != nil && err != git.NoErrAlreadyUpToDate {
		return nil, err
	}
	h, err := r.ResolveRevision(plumbing.Revision(rev))
	if err != nil {
		return nil, err
	}
	if h == nil {
		return nil, err
	}

	return r.CommitObject(*h)
}

func (r *repo) revisionTree(ctx context.Context, rev string) (*object.Tree, error) {
	c, err := r.revision(ctx, rev)

	if err != nil {
		return nil, err
	}

	rt, err := r.TreeObject(c.TreeHash)
	if err != nil {
		return nil, err
	}

	return rt.Tree("data")
}

func Mount(ctx context.Context, path, realPath string, chan TurnScore) error {
	repo := &repo{*r}
	dir, err := repo.revisionTree(ctx, revision)
	if err != nil {
		return err
	}

	to := new(time.Duration)
	*to = 10 * time.Second
	s, err := fs.Mount(path, &Root{repo: repo, defTree: dir}, &fs.Options{EntryTimeout: to})
	if err != nil {
		return err
	}
	go s.Serve()
	return nil
}
