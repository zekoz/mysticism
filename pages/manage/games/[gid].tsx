import type { GetServerSideProps, GetStaticProps, NextPage } from 'next'
import Link from 'next/link'
import { NextPageContext } from 'next';
import relayOptions, { relayOptionsAuth } from '../../../lib/relayOptions';
import { getClientEnvironment } from '../../../lib/client_environment';
import { Button, DatePicker, Form, Input, Select, Spin, Table } from 'antd';
import styles from '../styles/Games.module.css'
import { graphql } from 'relay-runtime';
import { EntryPointComponent, EntryPointContainer, useMutation, usePreloadedQuery } from 'react-relay';
import { RelayProps, withRelay } from 'relay-nextjs';
import Modal from 'antd/lib/modal/Modal';
import { Dispatch, SetStateAction, useContext, useEffect, useMemo, useState } from 'react';
import { CurrentUser, SessionContext } from '../../../lib/SessionContext';
import { NextRouter, useRouter } from 'next/router';
import { useForm } from 'antd/lib/form/Form';
import TextArea from 'antd/lib/input/TextArea';
import { GidManagerLaunchMutation } from '../../../lib/queries/__generated__/GidManagerLaunchMutation.graphql';
import { GidManagerQuery, GidManagerQuery$variables } from '../../../lib/queries/__generated__/GidManagerQuery.graphql';
import moment from 'moment';

const ROOT_QUERY = graphql`
  query GidManagerQuery($id: ID!) {
    node(id: $id) {
        ... on Game {
          id
          name
          state
          createTime
          description
          type
          ruleset
          turn
          script
          scheduledStart
        }
    }
  }
`;
function GameManager({ preloadedQuery }: RelayProps<{}, GidManagerQuery>) {
    const { node } = usePreloadedQuery(ROOT_QUERY, preloadedQuery);

    const [launchGame, launchInFlight] = useMutation<GidManagerLaunchMutation>(graphql`
        mutation GidManagerLaunchMutation($id: ID!) {
            startGame(id: $id) {
                id
            }
        }
    `);

    const [launched, setLaunched] = useState(false);

    const [form] = useForm();

    return (<div style={{ maxWidth: "40rem", padding: "2rem" }}>

        <Button
            disabled={launched}
            loading={launchInFlight}
            onClick={() => {
                launchGame({
                    variables: { id: node?.id ?? "" },
                    onCompleted: ({ startGame }) => {
                        setLaunched(true);
                    }
                })
            }}
        >
            Launch Game
        </Button>

        <Form
            form={form}
            layout="vertical"
            name="login"
            initialValues={{ ...node, scheduledStart: moment(node?.scheduledStart) }}
            preserve={false}
        >
            <Form.Item
                label="Name"
                name="name"
                preserve={false}
                rules={[{ required: true, message: 'Please input game name' }]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="Description"
                name="description"
                preserve={false}
                rules={[{ required: true, message: 'Please input game description' }]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="Ruleset"
                name="ruleset"
                preserve={false}
                rules={[{ required: true, message: 'Please input ruleset' }]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="Scheduled Start"
                name="scheduledStart"
                preserve={false}
            >
                <DatePicker showTime={true} />
            </Form.Item>
            <Form.Item
                label="Server script"
                name="script"
                preserve={false}
            >
                <TextArea rows={10} size="large" />
            </Form.Item>
        </Form>

    </div>);
}

const variablesFromContext: (router: NextRouter | NextPageContext) => GidManagerQuery$variables = ({ query }) => {
    return {
        id: (query.gid as string) ?? "",
    };
};

export default withRelay(GameManager, ROOT_QUERY, { ...relayOptionsAuth, variablesFromContext });
