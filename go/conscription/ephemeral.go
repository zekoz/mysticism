package conscription

import "flag"

var (
	poolSize = flag.Int("ephemeral_pool", 50, "Maximum number of ephemeral servers to manage")
	hotSize  = flag.Int("ephemeral_pregame", 2, "Number of ephemeral servers to keep in pregame")
)
