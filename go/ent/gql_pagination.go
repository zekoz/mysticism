// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"mysticism/ent/game"
	"mysticism/ent/group"
	"mysticism/ent/player"
	"mysticism/ent/server"
	"mysticism/ent/user"
	"strconv"
	"strings"

	"entgo.io/ent/dialect/sql"
	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/errcode"
	"github.com/vektah/gqlparser/v2/gqlerror"
	"github.com/vmihailenco/msgpack/v5"
)

// OrderDirection defines the directions in which to order a list of items.
type OrderDirection string

const (
	// OrderDirectionAsc specifies an ascending order.
	OrderDirectionAsc OrderDirection = "ASC"
	// OrderDirectionDesc specifies a descending order.
	OrderDirectionDesc OrderDirection = "DESC"
)

// Validate the order direction value.
func (o OrderDirection) Validate() error {
	if o != OrderDirectionAsc && o != OrderDirectionDesc {
		return fmt.Errorf("%s is not a valid OrderDirection", o)
	}
	return nil
}

// String implements fmt.Stringer interface.
func (o OrderDirection) String() string {
	return string(o)
}

// MarshalGQL implements graphql.Marshaler interface.
func (o OrderDirection) MarshalGQL(w io.Writer) {
	io.WriteString(w, strconv.Quote(o.String()))
}

// UnmarshalGQL implements graphql.Unmarshaler interface.
func (o *OrderDirection) UnmarshalGQL(val interface{}) error {
	str, ok := val.(string)
	if !ok {
		return fmt.Errorf("order direction %T must be a string", val)
	}
	*o = OrderDirection(str)
	return o.Validate()
}

func (o OrderDirection) reverse() OrderDirection {
	if o == OrderDirectionDesc {
		return OrderDirectionAsc
	}
	return OrderDirectionDesc
}

func (o OrderDirection) orderFunc(field string) OrderFunc {
	if o == OrderDirectionDesc {
		return Desc(field)
	}
	return Asc(field)
}

func cursorsToPredicates(direction OrderDirection, after, before *Cursor, field, idField string) []func(s *sql.Selector) {
	var predicates []func(s *sql.Selector)
	if after != nil {
		if after.Value != nil {
			var predicate func([]string, ...interface{}) *sql.Predicate
			if direction == OrderDirectionAsc {
				predicate = sql.CompositeGT
			} else {
				predicate = sql.CompositeLT
			}
			predicates = append(predicates, func(s *sql.Selector) {
				s.Where(predicate(
					s.Columns(field, idField),
					after.Value, after.ID,
				))
			})
		} else {
			var predicate func(string, interface{}) *sql.Predicate
			if direction == OrderDirectionAsc {
				predicate = sql.GT
			} else {
				predicate = sql.LT
			}
			predicates = append(predicates, func(s *sql.Selector) {
				s.Where(predicate(
					s.C(idField),
					after.ID,
				))
			})
		}
	}
	if before != nil {
		if before.Value != nil {
			var predicate func([]string, ...interface{}) *sql.Predicate
			if direction == OrderDirectionAsc {
				predicate = sql.CompositeLT
			} else {
				predicate = sql.CompositeGT
			}
			predicates = append(predicates, func(s *sql.Selector) {
				s.Where(predicate(
					s.Columns(field, idField),
					before.Value, before.ID,
				))
			})
		} else {
			var predicate func(string, interface{}) *sql.Predicate
			if direction == OrderDirectionAsc {
				predicate = sql.LT
			} else {
				predicate = sql.GT
			}
			predicates = append(predicates, func(s *sql.Selector) {
				s.Where(predicate(
					s.C(idField),
					before.ID,
				))
			})
		}
	}
	return predicates
}

// PageInfo of a connection type.
type PageInfo struct {
	HasNextPage     bool    `json:"hasNextPage"`
	HasPreviousPage bool    `json:"hasPreviousPage"`
	StartCursor     *Cursor `json:"startCursor"`
	EndCursor       *Cursor `json:"endCursor"`
}

// Cursor of an edge type.
type Cursor struct {
	ID    int   `msgpack:"i"`
	Value Value `msgpack:"v,omitempty"`
}

// MarshalGQL implements graphql.Marshaler interface.
func (c Cursor) MarshalGQL(w io.Writer) {
	quote := []byte{'"'}
	w.Write(quote)
	defer w.Write(quote)
	wc := base64.NewEncoder(base64.RawStdEncoding, w)
	defer wc.Close()
	_ = msgpack.NewEncoder(wc).Encode(c)
}

// UnmarshalGQL implements graphql.Unmarshaler interface.
func (c *Cursor) UnmarshalGQL(v interface{}) error {
	s, ok := v.(string)
	if !ok {
		return fmt.Errorf("%T is not a string", v)
	}
	if err := msgpack.NewDecoder(
		base64.NewDecoder(
			base64.RawStdEncoding,
			strings.NewReader(s),
		),
	).Decode(c); err != nil {
		return fmt.Errorf("cannot decode cursor: %w", err)
	}
	return nil
}

const errInvalidPagination = "INVALID_PAGINATION"

func validateFirstLast(first, last *int) (err *gqlerror.Error) {
	switch {
	case first != nil && last != nil:
		err = &gqlerror.Error{
			Message: "Passing both `first` and `last` to paginate a connection is not supported.",
		}
	case first != nil && *first < 0:
		err = &gqlerror.Error{
			Message: "`first` on a connection cannot be less than zero.",
		}
		errcode.Set(err, errInvalidPagination)
	case last != nil && *last < 0:
		err = &gqlerror.Error{
			Message: "`last` on a connection cannot be less than zero.",
		}
		errcode.Set(err, errInvalidPagination)
	}
	return err
}

func getCollectedField(ctx context.Context, path ...string) *graphql.CollectedField {
	fc := graphql.GetFieldContext(ctx)
	if fc == nil {
		return nil
	}
	oc := graphql.GetOperationContext(ctx)
	field := fc.Field

walk:
	for _, name := range path {
		for _, f := range graphql.CollectFields(oc, field.Selections, nil) {
			if f.Name == name {
				field = f
				continue walk
			}
		}
		return nil
	}
	return &field
}

func hasCollectedField(ctx context.Context, path ...string) bool {
	if graphql.GetFieldContext(ctx) == nil {
		return true
	}
	return getCollectedField(ctx, path...) != nil
}

const (
	edgesField      = "edges"
	nodeField       = "node"
	pageInfoField   = "pageInfo"
	totalCountField = "totalCount"
)

// GameEdge is the edge representation of Game.
type GameEdge struct {
	Node   *Game  `json:"node"`
	Cursor Cursor `json:"cursor"`
}

// GameConnection is the connection containing edges to Game.
type GameConnection struct {
	Edges      []*GameEdge `json:"edges"`
	PageInfo   PageInfo    `json:"pageInfo"`
	TotalCount int         `json:"totalCount"`
}

// GamePaginateOption enables pagination customization.
type GamePaginateOption func(*gamePager) error

// WithGameOrder configures pagination ordering.
func WithGameOrder(order *GameOrder) GamePaginateOption {
	if order == nil {
		order = DefaultGameOrder
	}
	o := *order
	return func(pager *gamePager) error {
		if err := o.Direction.Validate(); err != nil {
			return err
		}
		if o.Field == nil {
			o.Field = DefaultGameOrder.Field
		}
		pager.order = &o
		return nil
	}
}

// WithGameFilter configures pagination filter.
func WithGameFilter(filter func(*GameQuery) (*GameQuery, error)) GamePaginateOption {
	return func(pager *gamePager) error {
		if filter == nil {
			return errors.New("GameQuery filter cannot be nil")
		}
		pager.filter = filter
		return nil
	}
}

type gamePager struct {
	order  *GameOrder
	filter func(*GameQuery) (*GameQuery, error)
}

func newGamePager(opts []GamePaginateOption) (*gamePager, error) {
	pager := &gamePager{}
	for _, opt := range opts {
		if err := opt(pager); err != nil {
			return nil, err
		}
	}
	if pager.order == nil {
		pager.order = DefaultGameOrder
	}
	return pager, nil
}

func (p *gamePager) applyFilter(query *GameQuery) (*GameQuery, error) {
	if p.filter != nil {
		return p.filter(query)
	}
	return query, nil
}

func (p *gamePager) toCursor(ga *Game) Cursor {
	return p.order.Field.toCursor(ga)
}

func (p *gamePager) applyCursors(query *GameQuery, after, before *Cursor) *GameQuery {
	for _, predicate := range cursorsToPredicates(
		p.order.Direction, after, before,
		p.order.Field.field, DefaultGameOrder.Field.field,
	) {
		query = query.Where(predicate)
	}
	return query
}

func (p *gamePager) applyOrder(query *GameQuery, reverse bool) *GameQuery {
	direction := p.order.Direction
	if reverse {
		direction = direction.reverse()
	}
	query = query.Order(direction.orderFunc(p.order.Field.field))
	if p.order.Field != DefaultGameOrder.Field {
		query = query.Order(direction.orderFunc(DefaultGameOrder.Field.field))
	}
	return query
}

// Paginate executes the query and returns a relay based cursor connection to Game.
func (ga *GameQuery) Paginate(
	ctx context.Context, after *Cursor, first *int,
	before *Cursor, last *int, opts ...GamePaginateOption,
) (*GameConnection, error) {
	if err := validateFirstLast(first, last); err != nil {
		return nil, err
	}
	pager, err := newGamePager(opts)
	if err != nil {
		return nil, err
	}

	if ga, err = pager.applyFilter(ga); err != nil {
		return nil, err
	}

	conn := &GameConnection{Edges: []*GameEdge{}}
	if !hasCollectedField(ctx, edgesField) || first != nil && *first == 0 || last != nil && *last == 0 {
		if hasCollectedField(ctx, totalCountField) ||
			hasCollectedField(ctx, pageInfoField) {
			count, err := ga.Count(ctx)
			if err != nil {
				return nil, err
			}
			conn.TotalCount = count
			conn.PageInfo.HasNextPage = first != nil && count > 0
			conn.PageInfo.HasPreviousPage = last != nil && count > 0
		}
		return conn, nil
	}

	if (after != nil || first != nil || before != nil || last != nil) && hasCollectedField(ctx, totalCountField) {
		count, err := ga.Clone().Count(ctx)
		if err != nil {
			return nil, err
		}
		conn.TotalCount = count
	}

	ga = pager.applyCursors(ga, after, before)
	ga = pager.applyOrder(ga, last != nil)
	var limit int
	if first != nil {
		limit = *first + 1
	} else if last != nil {
		limit = *last + 1
	}
	if limit > 0 {
		ga = ga.Limit(limit)
	}

	if field := getCollectedField(ctx, edgesField, nodeField); field != nil {
		ga = ga.collectField(graphql.GetOperationContext(ctx), *field)
	}

	nodes, err := ga.All(ctx)
	if err != nil || len(nodes) == 0 {
		return conn, err
	}

	if len(nodes) == limit {
		conn.PageInfo.HasNextPage = first != nil
		conn.PageInfo.HasPreviousPage = last != nil
		nodes = nodes[:len(nodes)-1]
	}

	var nodeAt func(int) *Game
	if last != nil {
		n := len(nodes) - 1
		nodeAt = func(i int) *Game {
			return nodes[n-i]
		}
	} else {
		nodeAt = func(i int) *Game {
			return nodes[i]
		}
	}

	conn.Edges = make([]*GameEdge, len(nodes))
	for i := range nodes {
		node := nodeAt(i)
		conn.Edges[i] = &GameEdge{
			Node:   node,
			Cursor: pager.toCursor(node),
		}
	}

	conn.PageInfo.StartCursor = &conn.Edges[0].Cursor
	conn.PageInfo.EndCursor = &conn.Edges[len(conn.Edges)-1].Cursor
	if conn.TotalCount == 0 {
		conn.TotalCount = len(nodes)
	}

	return conn, nil
}

// GameOrderField defines the ordering field of Game.
type GameOrderField struct {
	field    string
	toCursor func(*Game) Cursor
}

// GameOrder defines the ordering of Game.
type GameOrder struct {
	Direction OrderDirection  `json:"direction"`
	Field     *GameOrderField `json:"field"`
}

// DefaultGameOrder is the default ordering of Game.
var DefaultGameOrder = &GameOrder{
	Direction: OrderDirectionAsc,
	Field: &GameOrderField{
		field: game.FieldID,
		toCursor: func(ga *Game) Cursor {
			return Cursor{ID: ga.ID}
		},
	},
}

// ToEdge converts Game into GameEdge.
func (ga *Game) ToEdge(order *GameOrder) *GameEdge {
	if order == nil {
		order = DefaultGameOrder
	}
	return &GameEdge{
		Node:   ga,
		Cursor: order.Field.toCursor(ga),
	}
}

// GroupEdge is the edge representation of Group.
type GroupEdge struct {
	Node   *Group `json:"node"`
	Cursor Cursor `json:"cursor"`
}

// GroupConnection is the connection containing edges to Group.
type GroupConnection struct {
	Edges      []*GroupEdge `json:"edges"`
	PageInfo   PageInfo     `json:"pageInfo"`
	TotalCount int          `json:"totalCount"`
}

// GroupPaginateOption enables pagination customization.
type GroupPaginateOption func(*groupPager) error

// WithGroupOrder configures pagination ordering.
func WithGroupOrder(order *GroupOrder) GroupPaginateOption {
	if order == nil {
		order = DefaultGroupOrder
	}
	o := *order
	return func(pager *groupPager) error {
		if err := o.Direction.Validate(); err != nil {
			return err
		}
		if o.Field == nil {
			o.Field = DefaultGroupOrder.Field
		}
		pager.order = &o
		return nil
	}
}

// WithGroupFilter configures pagination filter.
func WithGroupFilter(filter func(*GroupQuery) (*GroupQuery, error)) GroupPaginateOption {
	return func(pager *groupPager) error {
		if filter == nil {
			return errors.New("GroupQuery filter cannot be nil")
		}
		pager.filter = filter
		return nil
	}
}

type groupPager struct {
	order  *GroupOrder
	filter func(*GroupQuery) (*GroupQuery, error)
}

func newGroupPager(opts []GroupPaginateOption) (*groupPager, error) {
	pager := &groupPager{}
	for _, opt := range opts {
		if err := opt(pager); err != nil {
			return nil, err
		}
	}
	if pager.order == nil {
		pager.order = DefaultGroupOrder
	}
	return pager, nil
}

func (p *groupPager) applyFilter(query *GroupQuery) (*GroupQuery, error) {
	if p.filter != nil {
		return p.filter(query)
	}
	return query, nil
}

func (p *groupPager) toCursor(gr *Group) Cursor {
	return p.order.Field.toCursor(gr)
}

func (p *groupPager) applyCursors(query *GroupQuery, after, before *Cursor) *GroupQuery {
	for _, predicate := range cursorsToPredicates(
		p.order.Direction, after, before,
		p.order.Field.field, DefaultGroupOrder.Field.field,
	) {
		query = query.Where(predicate)
	}
	return query
}

func (p *groupPager) applyOrder(query *GroupQuery, reverse bool) *GroupQuery {
	direction := p.order.Direction
	if reverse {
		direction = direction.reverse()
	}
	query = query.Order(direction.orderFunc(p.order.Field.field))
	if p.order.Field != DefaultGroupOrder.Field {
		query = query.Order(direction.orderFunc(DefaultGroupOrder.Field.field))
	}
	return query
}

// Paginate executes the query and returns a relay based cursor connection to Group.
func (gr *GroupQuery) Paginate(
	ctx context.Context, after *Cursor, first *int,
	before *Cursor, last *int, opts ...GroupPaginateOption,
) (*GroupConnection, error) {
	if err := validateFirstLast(first, last); err != nil {
		return nil, err
	}
	pager, err := newGroupPager(opts)
	if err != nil {
		return nil, err
	}

	if gr, err = pager.applyFilter(gr); err != nil {
		return nil, err
	}

	conn := &GroupConnection{Edges: []*GroupEdge{}}
	if !hasCollectedField(ctx, edgesField) || first != nil && *first == 0 || last != nil && *last == 0 {
		if hasCollectedField(ctx, totalCountField) ||
			hasCollectedField(ctx, pageInfoField) {
			count, err := gr.Count(ctx)
			if err != nil {
				return nil, err
			}
			conn.TotalCount = count
			conn.PageInfo.HasNextPage = first != nil && count > 0
			conn.PageInfo.HasPreviousPage = last != nil && count > 0
		}
		return conn, nil
	}

	if (after != nil || first != nil || before != nil || last != nil) && hasCollectedField(ctx, totalCountField) {
		count, err := gr.Clone().Count(ctx)
		if err != nil {
			return nil, err
		}
		conn.TotalCount = count
	}

	gr = pager.applyCursors(gr, after, before)
	gr = pager.applyOrder(gr, last != nil)
	var limit int
	if first != nil {
		limit = *first + 1
	} else if last != nil {
		limit = *last + 1
	}
	if limit > 0 {
		gr = gr.Limit(limit)
	}

	if field := getCollectedField(ctx, edgesField, nodeField); field != nil {
		gr = gr.collectField(graphql.GetOperationContext(ctx), *field)
	}

	nodes, err := gr.All(ctx)
	if err != nil || len(nodes) == 0 {
		return conn, err
	}

	if len(nodes) == limit {
		conn.PageInfo.HasNextPage = first != nil
		conn.PageInfo.HasPreviousPage = last != nil
		nodes = nodes[:len(nodes)-1]
	}

	var nodeAt func(int) *Group
	if last != nil {
		n := len(nodes) - 1
		nodeAt = func(i int) *Group {
			return nodes[n-i]
		}
	} else {
		nodeAt = func(i int) *Group {
			return nodes[i]
		}
	}

	conn.Edges = make([]*GroupEdge, len(nodes))
	for i := range nodes {
		node := nodeAt(i)
		conn.Edges[i] = &GroupEdge{
			Node:   node,
			Cursor: pager.toCursor(node),
		}
	}

	conn.PageInfo.StartCursor = &conn.Edges[0].Cursor
	conn.PageInfo.EndCursor = &conn.Edges[len(conn.Edges)-1].Cursor
	if conn.TotalCount == 0 {
		conn.TotalCount = len(nodes)
	}

	return conn, nil
}

// GroupOrderField defines the ordering field of Group.
type GroupOrderField struct {
	field    string
	toCursor func(*Group) Cursor
}

// GroupOrder defines the ordering of Group.
type GroupOrder struct {
	Direction OrderDirection   `json:"direction"`
	Field     *GroupOrderField `json:"field"`
}

// DefaultGroupOrder is the default ordering of Group.
var DefaultGroupOrder = &GroupOrder{
	Direction: OrderDirectionAsc,
	Field: &GroupOrderField{
		field: group.FieldID,
		toCursor: func(gr *Group) Cursor {
			return Cursor{ID: gr.ID}
		},
	},
}

// ToEdge converts Group into GroupEdge.
func (gr *Group) ToEdge(order *GroupOrder) *GroupEdge {
	if order == nil {
		order = DefaultGroupOrder
	}
	return &GroupEdge{
		Node:   gr,
		Cursor: order.Field.toCursor(gr),
	}
}

// PlayerEdge is the edge representation of Player.
type PlayerEdge struct {
	Node   *Player `json:"node"`
	Cursor Cursor  `json:"cursor"`
}

// PlayerConnection is the connection containing edges to Player.
type PlayerConnection struct {
	Edges      []*PlayerEdge `json:"edges"`
	PageInfo   PageInfo      `json:"pageInfo"`
	TotalCount int           `json:"totalCount"`
}

// PlayerPaginateOption enables pagination customization.
type PlayerPaginateOption func(*playerPager) error

// WithPlayerOrder configures pagination ordering.
func WithPlayerOrder(order *PlayerOrder) PlayerPaginateOption {
	if order == nil {
		order = DefaultPlayerOrder
	}
	o := *order
	return func(pager *playerPager) error {
		if err := o.Direction.Validate(); err != nil {
			return err
		}
		if o.Field == nil {
			o.Field = DefaultPlayerOrder.Field
		}
		pager.order = &o
		return nil
	}
}

// WithPlayerFilter configures pagination filter.
func WithPlayerFilter(filter func(*PlayerQuery) (*PlayerQuery, error)) PlayerPaginateOption {
	return func(pager *playerPager) error {
		if filter == nil {
			return errors.New("PlayerQuery filter cannot be nil")
		}
		pager.filter = filter
		return nil
	}
}

type playerPager struct {
	order  *PlayerOrder
	filter func(*PlayerQuery) (*PlayerQuery, error)
}

func newPlayerPager(opts []PlayerPaginateOption) (*playerPager, error) {
	pager := &playerPager{}
	for _, opt := range opts {
		if err := opt(pager); err != nil {
			return nil, err
		}
	}
	if pager.order == nil {
		pager.order = DefaultPlayerOrder
	}
	return pager, nil
}

func (p *playerPager) applyFilter(query *PlayerQuery) (*PlayerQuery, error) {
	if p.filter != nil {
		return p.filter(query)
	}
	return query, nil
}

func (p *playerPager) toCursor(pl *Player) Cursor {
	return p.order.Field.toCursor(pl)
}

func (p *playerPager) applyCursors(query *PlayerQuery, after, before *Cursor) *PlayerQuery {
	for _, predicate := range cursorsToPredicates(
		p.order.Direction, after, before,
		p.order.Field.field, DefaultPlayerOrder.Field.field,
	) {
		query = query.Where(predicate)
	}
	return query
}

func (p *playerPager) applyOrder(query *PlayerQuery, reverse bool) *PlayerQuery {
	direction := p.order.Direction
	if reverse {
		direction = direction.reverse()
	}
	query = query.Order(direction.orderFunc(p.order.Field.field))
	if p.order.Field != DefaultPlayerOrder.Field {
		query = query.Order(direction.orderFunc(DefaultPlayerOrder.Field.field))
	}
	return query
}

// Paginate executes the query and returns a relay based cursor connection to Player.
func (pl *PlayerQuery) Paginate(
	ctx context.Context, after *Cursor, first *int,
	before *Cursor, last *int, opts ...PlayerPaginateOption,
) (*PlayerConnection, error) {
	if err := validateFirstLast(first, last); err != nil {
		return nil, err
	}
	pager, err := newPlayerPager(opts)
	if err != nil {
		return nil, err
	}

	if pl, err = pager.applyFilter(pl); err != nil {
		return nil, err
	}

	conn := &PlayerConnection{Edges: []*PlayerEdge{}}
	if !hasCollectedField(ctx, edgesField) || first != nil && *first == 0 || last != nil && *last == 0 {
		if hasCollectedField(ctx, totalCountField) ||
			hasCollectedField(ctx, pageInfoField) {
			count, err := pl.Count(ctx)
			if err != nil {
				return nil, err
			}
			conn.TotalCount = count
			conn.PageInfo.HasNextPage = first != nil && count > 0
			conn.PageInfo.HasPreviousPage = last != nil && count > 0
		}
		return conn, nil
	}

	if (after != nil || first != nil || before != nil || last != nil) && hasCollectedField(ctx, totalCountField) {
		count, err := pl.Clone().Count(ctx)
		if err != nil {
			return nil, err
		}
		conn.TotalCount = count
	}

	pl = pager.applyCursors(pl, after, before)
	pl = pager.applyOrder(pl, last != nil)
	var limit int
	if first != nil {
		limit = *first + 1
	} else if last != nil {
		limit = *last + 1
	}
	if limit > 0 {
		pl = pl.Limit(limit)
	}

	if field := getCollectedField(ctx, edgesField, nodeField); field != nil {
		pl = pl.collectField(graphql.GetOperationContext(ctx), *field)
	}

	nodes, err := pl.All(ctx)
	if err != nil || len(nodes) == 0 {
		return conn, err
	}

	if len(nodes) == limit {
		conn.PageInfo.HasNextPage = first != nil
		conn.PageInfo.HasPreviousPage = last != nil
		nodes = nodes[:len(nodes)-1]
	}

	var nodeAt func(int) *Player
	if last != nil {
		n := len(nodes) - 1
		nodeAt = func(i int) *Player {
			return nodes[n-i]
		}
	} else {
		nodeAt = func(i int) *Player {
			return nodes[i]
		}
	}

	conn.Edges = make([]*PlayerEdge, len(nodes))
	for i := range nodes {
		node := nodeAt(i)
		conn.Edges[i] = &PlayerEdge{
			Node:   node,
			Cursor: pager.toCursor(node),
		}
	}

	conn.PageInfo.StartCursor = &conn.Edges[0].Cursor
	conn.PageInfo.EndCursor = &conn.Edges[len(conn.Edges)-1].Cursor
	if conn.TotalCount == 0 {
		conn.TotalCount = len(nodes)
	}

	return conn, nil
}

// PlayerOrderField defines the ordering field of Player.
type PlayerOrderField struct {
	field    string
	toCursor func(*Player) Cursor
}

// PlayerOrder defines the ordering of Player.
type PlayerOrder struct {
	Direction OrderDirection    `json:"direction"`
	Field     *PlayerOrderField `json:"field"`
}

// DefaultPlayerOrder is the default ordering of Player.
var DefaultPlayerOrder = &PlayerOrder{
	Direction: OrderDirectionAsc,
	Field: &PlayerOrderField{
		field: player.FieldID,
		toCursor: func(pl *Player) Cursor {
			return Cursor{ID: pl.ID}
		},
	},
}

// ToEdge converts Player into PlayerEdge.
func (pl *Player) ToEdge(order *PlayerOrder) *PlayerEdge {
	if order == nil {
		order = DefaultPlayerOrder
	}
	return &PlayerEdge{
		Node:   pl,
		Cursor: order.Field.toCursor(pl),
	}
}

// ServerEdge is the edge representation of Server.
type ServerEdge struct {
	Node   *Server `json:"node"`
	Cursor Cursor  `json:"cursor"`
}

// ServerConnection is the connection containing edges to Server.
type ServerConnection struct {
	Edges      []*ServerEdge `json:"edges"`
	PageInfo   PageInfo      `json:"pageInfo"`
	TotalCount int           `json:"totalCount"`
}

// ServerPaginateOption enables pagination customization.
type ServerPaginateOption func(*serverPager) error

// WithServerOrder configures pagination ordering.
func WithServerOrder(order *ServerOrder) ServerPaginateOption {
	if order == nil {
		order = DefaultServerOrder
	}
	o := *order
	return func(pager *serverPager) error {
		if err := o.Direction.Validate(); err != nil {
			return err
		}
		if o.Field == nil {
			o.Field = DefaultServerOrder.Field
		}
		pager.order = &o
		return nil
	}
}

// WithServerFilter configures pagination filter.
func WithServerFilter(filter func(*ServerQuery) (*ServerQuery, error)) ServerPaginateOption {
	return func(pager *serverPager) error {
		if filter == nil {
			return errors.New("ServerQuery filter cannot be nil")
		}
		pager.filter = filter
		return nil
	}
}

type serverPager struct {
	order  *ServerOrder
	filter func(*ServerQuery) (*ServerQuery, error)
}

func newServerPager(opts []ServerPaginateOption) (*serverPager, error) {
	pager := &serverPager{}
	for _, opt := range opts {
		if err := opt(pager); err != nil {
			return nil, err
		}
	}
	if pager.order == nil {
		pager.order = DefaultServerOrder
	}
	return pager, nil
}

func (p *serverPager) applyFilter(query *ServerQuery) (*ServerQuery, error) {
	if p.filter != nil {
		return p.filter(query)
	}
	return query, nil
}

func (p *serverPager) toCursor(s *Server) Cursor {
	return p.order.Field.toCursor(s)
}

func (p *serverPager) applyCursors(query *ServerQuery, after, before *Cursor) *ServerQuery {
	for _, predicate := range cursorsToPredicates(
		p.order.Direction, after, before,
		p.order.Field.field, DefaultServerOrder.Field.field,
	) {
		query = query.Where(predicate)
	}
	return query
}

func (p *serverPager) applyOrder(query *ServerQuery, reverse bool) *ServerQuery {
	direction := p.order.Direction
	if reverse {
		direction = direction.reverse()
	}
	query = query.Order(direction.orderFunc(p.order.Field.field))
	if p.order.Field != DefaultServerOrder.Field {
		query = query.Order(direction.orderFunc(DefaultServerOrder.Field.field))
	}
	return query
}

// Paginate executes the query and returns a relay based cursor connection to Server.
func (s *ServerQuery) Paginate(
	ctx context.Context, after *Cursor, first *int,
	before *Cursor, last *int, opts ...ServerPaginateOption,
) (*ServerConnection, error) {
	if err := validateFirstLast(first, last); err != nil {
		return nil, err
	}
	pager, err := newServerPager(opts)
	if err != nil {
		return nil, err
	}

	if s, err = pager.applyFilter(s); err != nil {
		return nil, err
	}

	conn := &ServerConnection{Edges: []*ServerEdge{}}
	if !hasCollectedField(ctx, edgesField) || first != nil && *first == 0 || last != nil && *last == 0 {
		if hasCollectedField(ctx, totalCountField) ||
			hasCollectedField(ctx, pageInfoField) {
			count, err := s.Count(ctx)
			if err != nil {
				return nil, err
			}
			conn.TotalCount = count
			conn.PageInfo.HasNextPage = first != nil && count > 0
			conn.PageInfo.HasPreviousPage = last != nil && count > 0
		}
		return conn, nil
	}

	if (after != nil || first != nil || before != nil || last != nil) && hasCollectedField(ctx, totalCountField) {
		count, err := s.Clone().Count(ctx)
		if err != nil {
			return nil, err
		}
		conn.TotalCount = count
	}

	s = pager.applyCursors(s, after, before)
	s = pager.applyOrder(s, last != nil)
	var limit int
	if first != nil {
		limit = *first + 1
	} else if last != nil {
		limit = *last + 1
	}
	if limit > 0 {
		s = s.Limit(limit)
	}

	if field := getCollectedField(ctx, edgesField, nodeField); field != nil {
		s = s.collectField(graphql.GetOperationContext(ctx), *field)
	}

	nodes, err := s.All(ctx)
	if err != nil || len(nodes) == 0 {
		return conn, err
	}

	if len(nodes) == limit {
		conn.PageInfo.HasNextPage = first != nil
		conn.PageInfo.HasPreviousPage = last != nil
		nodes = nodes[:len(nodes)-1]
	}

	var nodeAt func(int) *Server
	if last != nil {
		n := len(nodes) - 1
		nodeAt = func(i int) *Server {
			return nodes[n-i]
		}
	} else {
		nodeAt = func(i int) *Server {
			return nodes[i]
		}
	}

	conn.Edges = make([]*ServerEdge, len(nodes))
	for i := range nodes {
		node := nodeAt(i)
		conn.Edges[i] = &ServerEdge{
			Node:   node,
			Cursor: pager.toCursor(node),
		}
	}

	conn.PageInfo.StartCursor = &conn.Edges[0].Cursor
	conn.PageInfo.EndCursor = &conn.Edges[len(conn.Edges)-1].Cursor
	if conn.TotalCount == 0 {
		conn.TotalCount = len(nodes)
	}

	return conn, nil
}

// ServerOrderField defines the ordering field of Server.
type ServerOrderField struct {
	field    string
	toCursor func(*Server) Cursor
}

// ServerOrder defines the ordering of Server.
type ServerOrder struct {
	Direction OrderDirection    `json:"direction"`
	Field     *ServerOrderField `json:"field"`
}

// DefaultServerOrder is the default ordering of Server.
var DefaultServerOrder = &ServerOrder{
	Direction: OrderDirectionAsc,
	Field: &ServerOrderField{
		field: server.FieldID,
		toCursor: func(s *Server) Cursor {
			return Cursor{ID: s.ID}
		},
	},
}

// ToEdge converts Server into ServerEdge.
func (s *Server) ToEdge(order *ServerOrder) *ServerEdge {
	if order == nil {
		order = DefaultServerOrder
	}
	return &ServerEdge{
		Node:   s,
		Cursor: order.Field.toCursor(s),
	}
}

// UserEdge is the edge representation of User.
type UserEdge struct {
	Node   *User  `json:"node"`
	Cursor Cursor `json:"cursor"`
}

// UserConnection is the connection containing edges to User.
type UserConnection struct {
	Edges      []*UserEdge `json:"edges"`
	PageInfo   PageInfo    `json:"pageInfo"`
	TotalCount int         `json:"totalCount"`
}

// UserPaginateOption enables pagination customization.
type UserPaginateOption func(*userPager) error

// WithUserOrder configures pagination ordering.
func WithUserOrder(order *UserOrder) UserPaginateOption {
	if order == nil {
		order = DefaultUserOrder
	}
	o := *order
	return func(pager *userPager) error {
		if err := o.Direction.Validate(); err != nil {
			return err
		}
		if o.Field == nil {
			o.Field = DefaultUserOrder.Field
		}
		pager.order = &o
		return nil
	}
}

// WithUserFilter configures pagination filter.
func WithUserFilter(filter func(*UserQuery) (*UserQuery, error)) UserPaginateOption {
	return func(pager *userPager) error {
		if filter == nil {
			return errors.New("UserQuery filter cannot be nil")
		}
		pager.filter = filter
		return nil
	}
}

type userPager struct {
	order  *UserOrder
	filter func(*UserQuery) (*UserQuery, error)
}

func newUserPager(opts []UserPaginateOption) (*userPager, error) {
	pager := &userPager{}
	for _, opt := range opts {
		if err := opt(pager); err != nil {
			return nil, err
		}
	}
	if pager.order == nil {
		pager.order = DefaultUserOrder
	}
	return pager, nil
}

func (p *userPager) applyFilter(query *UserQuery) (*UserQuery, error) {
	if p.filter != nil {
		return p.filter(query)
	}
	return query, nil
}

func (p *userPager) toCursor(u *User) Cursor {
	return p.order.Field.toCursor(u)
}

func (p *userPager) applyCursors(query *UserQuery, after, before *Cursor) *UserQuery {
	for _, predicate := range cursorsToPredicates(
		p.order.Direction, after, before,
		p.order.Field.field, DefaultUserOrder.Field.field,
	) {
		query = query.Where(predicate)
	}
	return query
}

func (p *userPager) applyOrder(query *UserQuery, reverse bool) *UserQuery {
	direction := p.order.Direction
	if reverse {
		direction = direction.reverse()
	}
	query = query.Order(direction.orderFunc(p.order.Field.field))
	if p.order.Field != DefaultUserOrder.Field {
		query = query.Order(direction.orderFunc(DefaultUserOrder.Field.field))
	}
	return query
}

// Paginate executes the query and returns a relay based cursor connection to User.
func (u *UserQuery) Paginate(
	ctx context.Context, after *Cursor, first *int,
	before *Cursor, last *int, opts ...UserPaginateOption,
) (*UserConnection, error) {
	if err := validateFirstLast(first, last); err != nil {
		return nil, err
	}
	pager, err := newUserPager(opts)
	if err != nil {
		return nil, err
	}

	if u, err = pager.applyFilter(u); err != nil {
		return nil, err
	}

	conn := &UserConnection{Edges: []*UserEdge{}}
	if !hasCollectedField(ctx, edgesField) || first != nil && *first == 0 || last != nil && *last == 0 {
		if hasCollectedField(ctx, totalCountField) ||
			hasCollectedField(ctx, pageInfoField) {
			count, err := u.Count(ctx)
			if err != nil {
				return nil, err
			}
			conn.TotalCount = count
			conn.PageInfo.HasNextPage = first != nil && count > 0
			conn.PageInfo.HasPreviousPage = last != nil && count > 0
		}
		return conn, nil
	}

	if (after != nil || first != nil || before != nil || last != nil) && hasCollectedField(ctx, totalCountField) {
		count, err := u.Clone().Count(ctx)
		if err != nil {
			return nil, err
		}
		conn.TotalCount = count
	}

	u = pager.applyCursors(u, after, before)
	u = pager.applyOrder(u, last != nil)
	var limit int
	if first != nil {
		limit = *first + 1
	} else if last != nil {
		limit = *last + 1
	}
	if limit > 0 {
		u = u.Limit(limit)
	}

	if field := getCollectedField(ctx, edgesField, nodeField); field != nil {
		u = u.collectField(graphql.GetOperationContext(ctx), *field)
	}

	nodes, err := u.All(ctx)
	if err != nil || len(nodes) == 0 {
		return conn, err
	}

	if len(nodes) == limit {
		conn.PageInfo.HasNextPage = first != nil
		conn.PageInfo.HasPreviousPage = last != nil
		nodes = nodes[:len(nodes)-1]
	}

	var nodeAt func(int) *User
	if last != nil {
		n := len(nodes) - 1
		nodeAt = func(i int) *User {
			return nodes[n-i]
		}
	} else {
		nodeAt = func(i int) *User {
			return nodes[i]
		}
	}

	conn.Edges = make([]*UserEdge, len(nodes))
	for i := range nodes {
		node := nodeAt(i)
		conn.Edges[i] = &UserEdge{
			Node:   node,
			Cursor: pager.toCursor(node),
		}
	}

	conn.PageInfo.StartCursor = &conn.Edges[0].Cursor
	conn.PageInfo.EndCursor = &conn.Edges[len(conn.Edges)-1].Cursor
	if conn.TotalCount == 0 {
		conn.TotalCount = len(nodes)
	}

	return conn, nil
}

// UserOrderField defines the ordering field of User.
type UserOrderField struct {
	field    string
	toCursor func(*User) Cursor
}

// UserOrder defines the ordering of User.
type UserOrder struct {
	Direction OrderDirection  `json:"direction"`
	Field     *UserOrderField `json:"field"`
}

// DefaultUserOrder is the default ordering of User.
var DefaultUserOrder = &UserOrder{
	Direction: OrderDirectionAsc,
	Field: &UserOrderField{
		field: user.FieldID,
		toCursor: func(u *User) Cursor {
			return Cursor{ID: u.ID}
		},
	},
}

// ToEdge converts User into UserEdge.
func (u *User) ToEdge(order *UserOrder) *UserEdge {
	if order == nil {
		order = DefaultUserOrder
	}
	return &UserEdge{
		Node:   u,
		Cursor: order.Field.toCursor(u),
	}
}
