import { graphql } from 'graphql';
import { IncomingHttpHeaders } from 'http';
import { NextPageContext } from 'next';
import { withHydrateDatetime } from 'relay-nextjs/date';
import { Environment, GraphQLResponse, Network, Store, RecordSource } from 'relay-runtime';

export function createServerNetwork(headers : IncomingHttpHeaders) {
  return Network.create(async (params, variables) => {
    const response = await fetch(
      'http://localhost:8888/graphql',
      {
        method: 'POST',
        headers: {
          'Cookie': headers.cookie ?? "",
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          query: params.text,
          variables,
        }),
      }
    );

    return await response.json();
  });
}

export function createServerEnvironment({req} : NextPageContext) {
  return new Environment({
    network: createServerNetwork(req?.headers ?? {}),
    store: new Store(new RecordSource()),
    isServer: true,
  });
}
