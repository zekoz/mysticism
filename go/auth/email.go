package auth

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"html/template"
	"mysticism"
	"mysticism/ent"
	"net/http"
	"net/smtp"
	"strconv"
	"strings"
	"time"

	mailyak "github.com/domodwyer/mailyak/v3"
)

const (
	maxAge              = 72 * time.Hour
	host                = "freecivweb.com"
	mailPort            = 587
	sender              = "postmaster@freecivweb.com"
	mailPassword        = "allniggers"
	plainTemplateString = `Welcome to Freeciv!

Click the following link to confirm and activate your new account:
https://freecivweb.com/activate/{{.Code}}

If the above link is not clickable, try copying and pasting it into the address bar of your web browser.`

	htmlTemplateString = `<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US"><head>
<meta http-equiv="Content-type" name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width">
<!-- prevent ios zooming + autoscaling -->
<meta name="x-apple-disable-message-reformatting">
<title></title>
</head>
  
<body style="text-align:left;" dir="ltr">

<!--[if mso]>
<style type="text/css">
body, table, td, th, h1, h2, h3 {font-family: Helvetica, Arial, sans-serif !important;}
</style>
<![endif]-->


	
<p>Welcome to Freeciv!</p>
<p>Click the following link to confirm and activate your new account:<br>
<a href="https://freecivweb.com/activate/{{.Code}}" target="_blank" rel="noopener" style="text-decoration: none; font-weight: bold; color: #006699;">https://freecivweb.com/activate/{{.Code}}</a></p>
<p>If the above link is not clickable, try copying and pasting it into the address bar of your web browser.</p>
  
  
<!-- prevent Gmail on iOS font size manipulation -->
<div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>



</body></html>`
)

var (
	plainTemplate = template.Must(template.New("plain").Parse(plainTemplateString))
	htmlTemplate  = template.Must(template.New("html").Parse(htmlTemplateString))
	macKey        = must(base64.StdEncoding.DecodeString("IiAiGqx2TdjiizYvMjDdCDVFUT06br/JES2P3irmRqo="))
	ErrMac        = errors.New("Mac doesn't match")
	ErrExpired    = errors.New("Code expired")
)

func must(bs []byte, err error) []byte {
	if err != nil {
		panic(err)
	}
	return bs
}

func generateCode(uid int, email string) string {
	t := uint32(time.Now().Unix())
	h := hmac.New(sha256.New, macKey)
	buf := make([]byte, len(email)+8)

	binary.BigEndian.PutUint32(buf, t)
	binary.BigEndian.PutUint32(buf[4:], uint32(uid))

	copy(buf[8:], []byte(email))
	h.Write(buf)
	s := h.Sum(nil)
	binary.BigEndian.PutUint32(s, t)
	binary.BigEndian.PutUint32(s[4:], uint32(uid))

	return base64.StdEncoding.EncodeToString(s[:18])
}

func verifyCode(ctx context.Context, client *ent.Client, code string) (*ent.User, error) {
	bs, err := base64.StdEncoding.DecodeString(code)
	if err != nil {
		return nil, err
	}

	u, err := client.User.Get(ctx, int(binary.BigEndian.Uint32(bs[4:])))
	if err != nil {
		return nil, err
	}

	h := hmac.New(sha256.New, macKey)
	h.Write(bs[0:8])
	h.Write([]byte(u.Email))
	s := h.Sum(nil)

	if !bytes.Equal(s[8:18], bs[8:18]) {
		return nil, ErrMac
	}
	t := time.Unix(int64(binary.BigEndian.Uint32(bs)), 0)
	if time.Since(t) > maxAge {
		return nil, ErrExpired
	}
	return u, nil
}

func ConfirmEmail(user *ent.User) error {
	mail := mailyak.New(host+":"+strconv.FormatInt(mailPort, 10),
		smtp.PlainAuth("", sender, mailPassword, host))

	mail.To(user.Email)
	mail.From(sender)
	mail.Subject("[Freeciv] Confirm your new account")
	data := struct{ Code string }{Code: generateCode(user.ID, user.Email)}
	if err := plainTemplate.Execute(mail.Plain(), &data); err != nil {
		return err
	}
	if err := htmlTemplate.Execute(mail.HTML(), &data); err != nil {
		return err
	}
	return mail.Send()
}

func Activate(w http.ResponseWriter, r *http.Request) {
	sp := strings.Split(r.URL.Path, "/")
	u, err := verifyCode(r.Context(), mysticism.Client, sp[len(sp)-1])
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	_, err = mysticism.Client.User.UpdateOne(u).SetVerified(true).Save(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
	http.Redirect(w, r, "https://freecivweb.com/games", http.StatusMovedPermanently)
}
