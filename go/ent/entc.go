// Copyright 2019-present Facebook Inc. All rights reserved.
// This source code is licensed under the Apache 2.0 license found
// in the LICENSE file in the root directory of this source tree.

//go:build ignore
// +build ignore

package main

import (
	"log"

	_ "embed"
	"entgo.io/contrib/entgql"
	"entgo.io/ent/entc"
	"entgo.io/ent/entc/gen"
)

//go:embed enum.tmpl
var et string

func main() {
	enumTemplate := gen.MustParse(gen.NewTemplate("enum.tmpl").
		Funcs(gen.Funcs).
		Funcs(entgql.TemplateFuncs).
		Parse(et))
	ex, err := entgql.NewExtension(
		entgql.WithWhereFilters(true),
		entgql.WithSchemaPath("../ent.graphql"),
		entgql.WithConfigPath("../gqlgen.yml"),
		entgql.WithTemplates(
			entgql.CollectionTemplate,
			enumTemplate,
			entgql.NodeTemplate,
			entgql.PaginationTemplate,
			entgql.TransactionTemplate,
			entgql.EdgeTemplate,
		),
	)
	if err != nil {
		log.Fatalf("creating entgql extension: %v", err)
	}
	opts := []entc.Option{
		entc.Extensions(ex),
		entc.FeatureNames("privacy"),
	}

	if err := entc.Generate("./schema", &gen.Config{}, opts...); err != nil {
		log.Fatalf("running ent codegen: %v", err)
	}
}
