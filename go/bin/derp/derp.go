package main

import (
	"context"
	"flag"
	"log"
	"mysticism"
	"mysticism/conscription"
	"mysticism/ent"

	"github.com/docker/docker/client"
	_ "github.com/lib/pq"
)

var (
	gameID = flag.Int("game", 4294967304, "gameid")
	userID = flag.Int("user", 5, "userid")
	leader = flag.String("leader", "Ernac", "userid")
)

func main() {
	flag.Parse()
	ctx := context.Background()
	c, err := client.NewClientWithOpts(client.WithHost("unix:///run/fcweb/podman.sock"))
	if err != nil {
		log.Fatal(err)
	}
	mysticism.Docker = c

	client, err := ent.Open("postgres", "user=fcweb dbname=fcweb host=/var/run/postgresql")
	if err != nil {
		log.Fatal(err)
	}

	mysticism.Client = client

	g, err := mysticism.Client.Game.Get(ctx, *gameID)
	u, err := mysticism.Client.User.Get(ctx, *userID)

	conscription.TakeHack(ctx, g, u, *leader)
}
