/**
 * @generated SignedSource<<35024143952e4b5dced307c43a69e9e3>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type GidManagerLaunchMutation$variables = {
  id: string;
};
export type GidManagerLaunchMutation$data = {
  readonly startGame: {
    readonly id: string;
  };
};
export type GidManagerLaunchMutation = {
  variables: GidManagerLaunchMutation$variables;
  response: GidManagerLaunchMutation$data;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      }
    ],
    "concreteType": "Game",
    "kind": "LinkedField",
    "name": "startGame",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "GidManagerLaunchMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "GidManagerLaunchMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "f35f98dbd599e97b5cec026eab8e55a5",
    "id": null,
    "metadata": {},
    "name": "GidManagerLaunchMutation",
    "operationKind": "mutation",
    "text": "mutation GidManagerLaunchMutation(\n  $id: ID!\n) {\n  startGame(id: $id) {\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "697fc2babb73edde2bf1bf5544f42a82";

export default node;
