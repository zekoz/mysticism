/**
 * @generated SignedSource<<d219f4651f847629b2ca093d9342826d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type GameState = "PLANNED" | "PREGAME" | "RUNNING" | "ENDED" | "COMPLETE" | "SUSPENDED" | "%future added value";
export type GameType = "SHORTTURN" | "LONGTURN" | "%future added value";
export type GameInput = {
  id?: string | null;
  name?: string | null;
  description?: string | null;
  type?: GameType | null;
  state?: GameState | null;
  build?: string | null;
  ruleset?: string | null;
  rulesetCommit?: string | null;
  script?: string | null;
  scheduledStart?: any | null;
};
export type gamesManager_createMutation$variables = {
  input: GameInput;
};
export type gamesManager_createMutation$data = {
  readonly createGame: {
    readonly id: string;
  };
};
export type gamesManager_createMutation = {
  variables: gamesManager_createMutation$variables;
  response: gamesManager_createMutation$data;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "Game",
    "kind": "LinkedField",
    "name": "createGame",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "gamesManager_createMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "gamesManager_createMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "2a877a5f0427edb82ff7d56efd23cd17",
    "id": null,
    "metadata": {},
    "name": "gamesManager_createMutation",
    "operationKind": "mutation",
    "text": "mutation gamesManager_createMutation(\n  $input: GameInput!\n) {\n  createGame(input: $input) {\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "4b833dfbf0a43a9e712f944b5ee482bc";

export default node;
