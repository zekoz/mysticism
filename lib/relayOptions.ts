import { NextPageContext } from 'next';
import { getClientEnvironment } from './client_environment';

export const relayOptions = {
  createClientEnvironment: () => getClientEnvironment()!,
  createServerEnvironment: async (ctx: NextPageContext) => {
    const { createServerEnvironment } = await import('./server/server_environment');
    return createServerEnvironment(ctx);
  },
};

export const relayOptionsAuth = {
  serverSideProps: async (ctx: NextPageContext) => {
    const {adminProps} = await import('./server/admin_props');
    return await adminProps(ctx);
  },
  createClientEnvironment: () => getClientEnvironment()!,
  createServerEnvironment: async (ctx: NextPageContext) => {
    const { createServerEnvironment } = await import('./server/server_environment');
    return createServerEnvironment(ctx);
  },
};

export default relayOptions;
