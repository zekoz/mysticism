import { createContext, Dispatch, SetStateAction } from "react";

export type CurrentUser = {
  id?: string
  name?: string
};

export type Session = {
  user: CurrentUser,
  setUser: Dispatch<SetStateAction<CurrentUser>>,
  openLoginModal: () => any,
  logout: () => any,
  initialSessionFetched: boolean,
  tryMatching: boolean,
  setTryMatching: Dispatch<SetStateAction<boolean>>,
}
  
export const SessionContext = createContext<Session | null>(null);
  